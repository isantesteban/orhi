#include <orhi/common.hh>

namespace orhi
{
    class Model;
    class Mesh;
}

namespace orhi::loaders
{
    std::shared_ptr<Model> LoadModel(std::string path);
    std::shared_ptr<Mesh> LoadMesh(std::string path);
}