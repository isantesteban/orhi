#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Container : public Widget
{
public:
    void add(std::shared_ptr<Widget> widget);
    void add(const std::vector<std::shared_ptr<Widget>> &widgets);

    void setSize(Vector2i size) override;
    void setSize(int width, int height) override;
    void setWidth(int width) override;
    void setHeight(int height) override;

    void setRelativeSize(Vector2f size) override;
    void setRelativeSize(float width, float height) override;
    void setRelativeWidth(float width) override;;
    void setRelativeHeight(float height) override;

    virtual void adaptSizeToContent();
    virtual void adaptWidthToContent();
    virtual void adaptHeightToContent();

    Container();
    Container(const std::vector<std::shared_ptr<Widget>> &widgets);

protected:
    std::vector<std::shared_ptr<Widget>> m_widgets;
    Vector2b m_automaticSizeAdapt;
};

} // namespace orhi::gui