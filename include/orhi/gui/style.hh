#pragma once

#include <orhi/common.hh>

struct GLFWwindow;

namespace orhi::gui
{

class Style
{
public:
    float getEM() const;
    void setEM(float em);

    float getScale() const;
    void setScale(float scale);

    static float GetEM();
    static Vector2i CalcTextSize(const std::string &text);
    static Color GetFontColor();

    Style();

private:
    float m_em;
    float m_scale;

    void apply();
};

} // namespace orhi::gui