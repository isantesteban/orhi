#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Slider : public Widget
{
public:
    Slider(float *value, Vector2f range, const std::string &label = "", std::function<void(float)> onChange = {});

private:
    float *m_value;
    Vector2f m_range;
    std::function<void(float)> m_onChange;
    std::string m_label;

    void render() override;
};

} // namespace orhi::gui