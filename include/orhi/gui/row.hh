#pragma once

#include <orhi/common.hh>
#include <orhi/gui/container.hh>

namespace orhi::gui
{

class Row : public Container
{
public:
    void render() override;
    void adaptWidthToContent() override;
    void adaptHeightToContent() override;

    Row();
    Row(const std::vector<std::shared_ptr<Widget>> &widgets);
};

} // namespace orhi::gui