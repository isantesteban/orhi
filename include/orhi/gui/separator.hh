#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Separator : public Widget
{
public:
    void render() override;
};

} // namespace orhi::gui