#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Combo : public Widget
{
public:
    Combo(const std::vector<std::string> &values,
          const std::string &label = "",
          std::function<void(std::string)> onChange = {},
          int currentIndex = 0);

private:
    std::vector<std::string> m_values;
    int m_currentIndex;
    std::function<void(std::string)> m_onChange;
    std::string m_label;

    void render() override;
};

} // namespace orhi::gui