#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi
{
class Scene;
class Camera;
} // namespace orhi

namespace orhi::graphics
{
class Renderer;
} // namespace orhi::graphics

namespace orhi::gui
{
    
class Viewport : public Widget
{
public:
    Viewport(std::shared_ptr<Scene> scene, std::shared_ptr<Camera> camera);

private:
    std::shared_ptr<Scene> m_scene;
    std::shared_ptr<Camera> m_camera;
    std::shared_ptr<graphics::Renderer> m_renderer;
    //std::shared_ptr<RenderTexture> m_renderTexture;
    // m_renderResolution

    void bind(std::shared_ptr<Application> application) override;
    void onPreRender() override {};
    void render() override;
    void onPostRender() override {};

    void onResize(int width, int height) override;
};

} // namespace orhi::gui
