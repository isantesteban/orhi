#pragma once

#include <orhi/common.hh>
#include <orhi/gui/container.hh>

namespace orhi::gui
{

class Panel : public Container
{
public:
    void adaptWidthToContent() override;
    void adaptHeightToContent() override;

    Panel();
    Panel(const std::string &title);

protected:
    void render() override;
    void onPreRender() override;
    void onPostRender() override;

private:
    std::string m_title;
    Vector2i m_titleSize;
    int m_flags;
};

} // namespace orhi::gui