#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Text : public Widget
{
public:
    Text(const std::string &text);
    Text(const std::string &text, Color color);
    //Text(const std::string &text, Color color, Font font);

private:
    std::string m_text;
    Color m_color;

    void render() override;
};

} // namespace orhi::gui