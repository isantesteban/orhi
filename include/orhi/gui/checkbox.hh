#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Checkbox : public Widget
{
public:
    Checkbox(const std::string &label = "", std::function<void(bool)> onChange = {});

private:
    bool m_isChecked; 
    std::string m_label;
    std::function<void(bool)> m_onChange;

    void render() override; 
};

} // namespace orhi::gui
