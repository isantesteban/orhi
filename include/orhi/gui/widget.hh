#pragma once

#include <orhi/common.hh>
#include <orhi/application/application.hh>

namespace orhi::gui
{

enum class Anchor
{
    TOP_LEFT,
    TOP_CENTER,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_CENTER,
    BOTTOM_RIGHT,
    CENTER_LEFT,
    CENTER_RIGHT,
    CENTER
};

class Widget
{
public:
    virtual void setEnabled(bool enabled);
    virtual void toggleEnabled();
    virtual bool isEnabled() const;

    virtual Anchor getAnchor() const;
    virtual void setAnchor(Anchor anchor);

    virtual Vector2i getPosition() const;
    virtual void setPosition(Vector2i position);
    virtual void setPosition(int x, int y);
    virtual void setPositionX(int x);
    virtual void setPositionY(int y);

    virtual Vector2f getRelativePosition() const;
    virtual void setRelativePosition(Vector2f position);
    virtual void setRelativePosition(float x, float y);
    virtual void setRelativePositionX(float x);
    virtual void setRelativePositionY(float y);

    virtual Vector2i getSize() const;
    virtual void setSize(Vector2i size);
    virtual void setSize(int width, int height);

    virtual int getWidth() const;
    virtual void setWidth(int width);

    virtual int getHeight() const;
    virtual void setHeight(int height);

    virtual Vector2f getRelativeSize() const;
    virtual void setRelativeSize(Vector2f size);
    virtual void setRelativeSize(float width, float height);

    virtual float getRelativeWidth() const;
    virtual void setRelativeWidth(float width);

    virtual float getRelativeHeight() const;
    virtual void setRelativeHeight(float height);

    virtual Vector3f getBackgroundColor() const;
    virtual void setBackgroundColor(Vector3f color);


    Widget();

protected:
    std::string m_id;

    Anchor m_anchor;
    Vector2i m_screenPosition;
    Vector2i m_position;
    Vector2f m_relativePosition;
    Vector2b m_useRelativePosition;

    Vector2i m_size;
    Vector2f m_relativeSize;
    Vector2b m_useRelativeSize;

    Vector3f m_backgroundColor; 

    bool m_enabled;
    bool m_insideContainer;

    virtual void bind(std::shared_ptr<Application> application);
   
    virtual void render() = 0;
    
    virtual void onPreRender();
    virtual void onPostRender();
    virtual void onResize(int width, int height);
    // virtual void onHover() {}; // Unimplemented
    // virtual void onPressed() {}; // Unimplemented
    // virtual void onFocus() {}; // Unimplemented

private:
    void recomputeSize(Vector2i windowSize);
    void recomputePosition(Vector2i windowSize);

    friend class Container;
    friend class Panel;
    friend class Row;
    friend class orhi::Application;
};

} // namespace orhi::gui