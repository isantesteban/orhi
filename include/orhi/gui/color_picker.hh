#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi
{
class Color;
} // namespace orhi

namespace orhi::gui
{

class ColorPicker : public Widget
{
public:
    ColorPicker(Color *color, const std::string &label = "", std::function<void(const Color &)> onChange = {});

private:
    Color *m_color;
    Vector4f m_value;
    std::string m_label;
    std::function<void(const Color &)> m_onChange;

    void render() override;
};

} // namespace orhi::gui