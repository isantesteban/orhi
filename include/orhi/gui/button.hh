#pragma once

#include <orhi/common.hh>
#include <orhi/gui/widget.hh>

namespace orhi::gui
{

class Button : public Widget
{
public:
    Button(const std::string &label = "", std::function<void()> onPressed = {});

protected:
    std::string m_label;
    std::function<void()> m_onPressed;

    void render() override;
};

class SmallButton : public Widget
{
public:
    SmallButton(const std::string &label = "", std::function<void()> onPressed = {});

protected:
    std::string m_label;
    std::function<void()> m_onPressed;

    void render() override;
};

} // namespace orhi::gui
