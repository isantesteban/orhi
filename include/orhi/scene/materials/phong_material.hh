#pragma once

#include <orhi/common.hh>
#include <orhi/scene/materials/material.hh>

namespace orhi
{

class PhongMaterial : public Material
{
public:
    Color getDiffuse() const;
    void setDiffuse(Color diffuse);

    Color getSpecular() const;
    void setSpecular(Color specular);

    Color getEmissive() const;
    void setEmissive(Color emissive);

    float getShininess() const;
    void setShininess(float shininess);

    bool isWireframeVisible();
    void setWireframeVisible(bool value);

    void setWireframeColor(Color color);
    Color getWireframeColor() const;

    void setWireframeThickness(float thickness);
    float getWireframeThickness() const;

    PhongMaterial();

private:
    Color m_diffuse;
    Color m_specular;
    Color m_emissive;
    float m_shininess;

    bool m_wireframeVisible;
    Color m_wireframeColor;
    float m_wireframeThickness;

    using Material::set;
};

} // namespace orhi