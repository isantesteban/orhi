#pragma once

#include <orhi/common.hh>
#include <orhi/graphics/resource.hh>

namespace orhi::graphics
{
class Shader;
}

namespace orhi
{

class Material : public graphics::Resource
{
public:
    void set(const std::string &key, float value) const;
    void set(const std::string &key, const Vector3f &value) const;
    void set(const std::string &key, const Vector4f &value) const;
    void set(const std::string &key, const Matrix4f &value) const;
    void set(const std::string &key, const Matrix3f &value) const;
    void set(const std::string &key, const Color &value) const;

    Material(const std::string &vertexShader, const std::string &fragmentShader);
    Material(const std::string &vertexShader, const std::string &geometryShader, const std::string &fragmentShader);
    Material(std::shared_ptr<graphics::Shader> vertexShader, std::shared_ptr<graphics::Shader> fragmentShader);
    Material(std::shared_ptr<graphics::Shader> vertexShader, std::shared_ptr<graphics::Shader> geometryShader, std::shared_ptr<graphics::Shader> fragmentShader);
    ~Material();

protected:
    std::shared_ptr<graphics::Shader> m_vertexShader;
    std::shared_ptr<graphics::Shader> m_geometryShader;
    std::shared_ptr<graphics::Shader> m_fragmentShader;
    std::map<std::string, int> m_uniformLocations;

    void linkShaders();
    void getUniforms();

    int getUniformLocation(const std::string &key) const;

    Material();
};

} // namespace orhi