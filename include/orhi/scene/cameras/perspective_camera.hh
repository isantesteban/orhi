#pragma once

#include <orhi/scene/cameras/camera.hh>

namespace orhi
{
class PerspectiveCamera : public Camera
{
public:
    float getFov() const;
    void setFov(float fov);

    bool isVisible(std::shared_ptr<const Node> node) const override;

    PerspectiveCamera();

private:
    float m_fov;

    void updateProjectionMatrix() override;
};

} // namespace orhi