#pragma once

#include <orhi/scene/cameras/camera.hh>

namespace orhi
{
class OrthographicCamera : public Camera
{
public:
    float getScale() const;
    void setScale(float scale);

    bool isVisible(std::shared_ptr<const Node> node) const override;

    OrthographicCamera();

private:
    float m_left;
    float m_right;
    float m_bottom;
    float m_top;
    float m_scale;

    void updateProjectionMatrix() override;
};

} // namespace orhi