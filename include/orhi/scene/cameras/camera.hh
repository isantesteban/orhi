#pragma once

#include <orhi/common.hh>
#include <orhi/scene/node.hh>

namespace orhi
{
class CameraControls;

class Camera : public Node
{
public:
    Matrix4f getProjectionMatrix() const;

    float getAspectRatio() const;
    void setAspectRatio(float aspectRatio);

    float getNear() const;
    void setNear(float near);

    float getFar() const;
    void setFar(float far);

    std::shared_ptr<CameraControls> getControls() const;
    void setControls(std::shared_ptr<CameraControls> controls);
    
    virtual bool isVisible(std::shared_ptr<const Node> node) const = 0;

    Camera() : m_aspectRatio(1.0f),
               m_near(0.1f),
               m_far(1000.0f),
               m_projectionMatrix(Matrix4f::Identity()){};

protected:
    float m_aspectRatio;
    float m_near;
    float m_far;

    Matrix4f m_projectionMatrix;
    
    virtual void updateProjectionMatrix() = 0;

private:
    std::shared_ptr<CameraControls> m_controls;
};

} // namespace orhi