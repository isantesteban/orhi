#pragma once

#include <orhi/common.hh>

namespace orhi
{
class Camera;
class EventManager;

class CameraControls
{   
public:
    virtual void bind(std::shared_ptr<Camera> camera, std::shared_ptr<EventManager> eventManager) = 0;
};

}