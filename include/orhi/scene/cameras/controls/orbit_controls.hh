#pragma once

#include <orhi/common.hh>
#include <orhi/scene/cameras/controls/camera_controls.hh>


namespace orhi
{
class OrthographicCamera;
    
class OrbitControls : public CameraControls
{
public:
    void bind(std::shared_ptr<Camera> camera, std::shared_ptr<EventManager> eventManager) override;

    OrbitControls() : m_orbitSpeed(0.003f),
                      m_truckSpeed(0.001f),
                      m_dollySpeed(0.15f),
                      m_orbitActivated(false),
                      m_truckActivated(false),
                      m_orbitCenter(Vector3f::Zero()),
                      m_cursorPosition(Vector2f::Zero()){};

private:
    bool m_orbitActivated;
    bool m_truckActivated;

    float m_orbitSpeed;
    float m_truckSpeed;
    float m_dollySpeed;

    Vector2f m_cursorPosition;
    Vector3f m_orbitCenter;

    void orbit(std::shared_ptr<Camera> camera, Vector2f cursorPosition);
    void truck(std::shared_ptr<Camera> camera, Vector2f cursorPosition);
    void dolly(std::shared_ptr<Camera> camera, Vector2f scrollOffset);
    void scale(std::shared_ptr<OrthographicCamera> camera, Vector2f scrollOffset);
};

} // namespace orhi