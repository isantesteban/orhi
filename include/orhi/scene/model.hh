#pragma once

#include <orhi/common.hh>
#include <orhi/scene/node.hh>

namespace orhi
{
class Mesh;
class Material;

class Model : public Node
{
public:
    std::shared_ptr<Mesh> getMesh() const;
    void setMesh(std::shared_ptr<Mesh> mesh);

    std::shared_ptr<Material> getMaterial() const;
    void setMaterial(std::shared_ptr<Material> material);

    Model(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material) : m_mesh(mesh),
                                                                            m_material(material){};

private:
    std::shared_ptr<Mesh> m_mesh;
    std::shared_ptr<Material> m_material;
};
} // namespace orhi