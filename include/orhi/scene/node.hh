#pragma once

#include <orhi/common.hh>

namespace orhi
{
enum class Space
{
    LOCAL,
    WORLD
};

class Node : public std::enable_shared_from_this<Node>
{
public:
    // Scene graph
    // ---------------------------------------------

    std::shared_ptr<Node> getParent() const;
    void setParent(std::shared_ptr<Node> parent);

    void add(std::shared_ptr<Node> child);
    void remove(std::shared_ptr<Node> child);
    std::vector<std::shared_ptr<Node>> getChildren() const;

    template <class T>
    std::vector<std::shared_ptr<T>> getChildrenByType() const
    {
        std::vector<std::shared_ptr<T>> result;

        for (auto const &child : m_children)
        {
            std::shared_ptr<T> castedChild = std::dynamic_pointer_cast<T>(child);
            if (castedChild != nullptr)
            {
                result.push_back(castedChild);
            }
            std::vector<std::shared_ptr<T>> childResult = child->getChildrenByType<T>();
            result.insert(result.end(), childResult.begin(), childResult.end());
        }

        return result;
    }

    // Transformations in 3D space
    // ---------------------------------------------

    Vector3f getPosition() const;
    void setPosition(Vector3f position);
    void setPosition(float x, float y, float z);

    Vector3f getWorldPosition() const;
    // void setWorldPosition(Vector3f position);
    // void setWorldPosition(float x, float y, float z);

    Vector3f getScale() const;
    void setScale(Vector3f scale);
    void setScale(float x, float y, float z);
    void setScale(float scale);

    // Quaternion getRotation() const;
    // void setRotation(Quaternion rotation);

    Vector3f getEulerAngles() const;
    void setEulerAngles(Vector3f eulerAngles);
    void setEulerAngles(float x, float y, float z);

    void setWorldTransform(AffineTransform transform);
    AffineTransform getWorldTransform() const;
    AffineTransform getWorldTransformInverse() const;

    void setLocalTransform(AffineTransform transform);
    AffineTransform getLocalTransform() const;
    AffineTransform getLocalTransformInverse() const;

    void translate(Vector3f translation, Space space = Space::WORLD);
    void translate(float x, float y, float z, Space space = Space::WORLD);
    void rotate(Vector3f axis, float angle, Space space = Space::WORLD);
    void rotateAround(Vector3f point, Vector3f axis, float angle);
    void lookAt(Vector3f point);

    Node() : m_localPosition(Vector3f::Zero()),
             m_localScale(Vector3f::Ones()),
             m_localRotation(Quaternion::Identity()),
             m_localTransform(AffineTransform::Identity()),
             m_localTransformInverse(AffineTransform::Identity()),
             m_worldTransform(AffineTransform::Identity()),
             m_worldTransformInverse(AffineTransform::Identity()){};

    virtual ~Node() {};

protected:
    std::shared_ptr<Node> m_parent;
    std::vector<std::shared_ptr<Node>> m_children;

    // TODO: Create own class Transform that encapsulates all this
    Vector3f m_localPosition;
    Vector3f m_localScale;
    Quaternion m_localRotation;

    AffineTransform m_localTransform;
    AffineTransform m_localTransformInverse;

    Vector3f m_worldPosition;
    Vector3f m_worldScale;
    Quaternion m_worldRotation;

    Vector3f m_up;
    Vector3f m_forward;
    Vector3f m_right;

    AffineTransform m_worldTransform;
    AffineTransform m_worldTransformInverse;

    void translateLocalSpace(Vector3f translation);
    void translateWorldSpace(Vector3f translation);

    void rotateLocalSpace(Vector3f axis, float angle);
    void rotateWorldSpace(Vector3f axis, float angle);

    void updateWorldTransform();
    void updateLocalTransform();

    void composeLocalTransform();
    void composeWorldTransform();

    void decomposeLocalTransform();
    void decomposeWorldTransform();
};

} // namespace orhi