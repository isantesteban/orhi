#pragma once

#include <orhi/common.hh>
#include <orhi/scene/node.hh>

namespace orhi
{

class Light : public Node
{
public:
    Color getColor() const;
    void setColor(Color color);

    float getIntensity() const;
    void setIntensity(float intensity);

private:
    Color m_color = Color::WHITE;
    float m_intensity = 1.0f; // TODO: https://seblagarde.files.wordpress.com/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf
};

} // namespace orhi