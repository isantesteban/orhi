#pragma once

#include <orhi/scene/lights/light.hh>

namespace orhi
{
    
class PointLight : public Light
{
public:
    float getRadius() const;
    void setRadius(float radius);

private:
    float m_radius = 100.0f; // TODO: define light radius in meters
};

}