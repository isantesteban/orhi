#pragma once

#include <orhi/common.hh>
#include <orhi/graphics/resource.hh>

namespace orhi::graphics
{
class Buffer;
}

namespace orhi
{
enum class MeshBuffer
{
    VERTEX_POSITIONS = 0,
    VERTEX_NORMALS = 1,
    VERTEX_TANGENTS = 2,
    VERTEX_TEXTURE_COORDINATES = 3,
    VERTEX_COLORS = 4,
    FACES
};

class Mesh : public graphics::Resource
{
public:
    void setFaces(const Eigen::Ref<const MatrixXi> &faces);
    MatrixXi &getFaces();
    int getNumFaces() const;

    void setVertices(const Eigen::Ref<const MatrixXf> &vertices);
    MatrixXf &getVertices();
    int getNumVertices() const;

    void setNormals(const Eigen::Ref<const MatrixXf> &normals);
    MatrixXf &getNormals();
    void computeNormals();

    void setColors(const Eigen::Ref<const MatrixXf> &colors);
    MatrixXf &getColors();

    void setTextureCoordinates(const Eigen::Ref<const MatrixXf> &uvs);
    MatrixXf &getTextureCoordinates();

    Mesh();
    Mesh(const Eigen::Ref<const MatrixXf> &vertices, const Eigen::Ref<const MatrixXi> &faces);
    ~Mesh();

private:
    MatrixXi m_faces;
    MatrixXf m_vertices;
    MatrixXf m_normals;
    MatrixXf m_tangents;
    MatrixXf m_colors;
    MatrixXf m_uvs;

    std::map<MeshBuffer, std::shared_ptr<graphics::Buffer>> m_buffers;

    std::shared_ptr<graphics::Buffer> getBuffer(MeshBuffer key);
};

} // namespace orhi