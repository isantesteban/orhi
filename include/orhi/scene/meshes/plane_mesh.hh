#pragma once

#include <orhi/scene/meshes/mesh.hh>

namespace orhi
{

class PlaneMesh : public Mesh
{
public:
    PlaneMesh(float width = 1.0f, float height = 1.0f, int segments = 1);
};

} // namespace orhi