#pragma once

#include <orhi/scene/meshes/mesh.hh>

namespace orhi
{

class BoxMesh : public Mesh
{
public:
    BoxMesh(float width=1.0f, float height=1.0f, float depth=1.0f, int segments=1);
};

} // namespace orhi