#pragma once

#include <orhi/scene/meshes/mesh.hh>

namespace orhi
{

class SphereMesh : public Mesh
{
public:
    SphereMesh(float radius = 1.0f, int segments = 32);
};

} // namespace orhi