#ifdef NDEBUG
#define IS_DEBUG_BUILD false
#else
#define DEBUG
#define IS_DEBUG_BUILD true
#endif

#define STRINGIFY(A) #A

#define MAKE_SINGLE_STATEMENT(...) \
    do                             \
    {                              \
        __VA_ARGS__                \
    } while (false)

#define LOG_INFO(message)  \
    MAKE_SINGLE_STATEMENT( \
        std::cout << "[ INFO ] " << message << std::endl;)

#define LOG_WARNING(message) \
    MAKE_SINGLE_STATEMENT(   \
        std::cout << "[ WARNING ] " << message << std::endl;)

#define LOG_ERROR(message)                                                           \
    MAKE_SINGLE_STATEMENT(                                                           \
        std::cout << "[ ERROR ] " << message << std::endl;                           \
        std::cout << "          Function: " << __FUNCTION__ << std::endl;            \
        std::cout << "          File: " << __FILE__ << ":" << __LINE__ << std::endl; \
        exit(-1);)

#define LOG_DEBUG(message)  \
    MAKE_SINGLE_STATEMENT(  \
        if (IS_DEBUG_BUILD) \
                std::cout   \
            << "[ DEBUG ] [" << __FILE__ << "(" << __LINE__ << ")] " << message << std::endl;)

#define LOG_RUNTIME(function)                                                               \
    MAKE_SINGLE_STATEMENT(                                                                  \
        using namespace std::chrono;                                                        \
        high_resolution_clock::time_point _start = high_resolution_clock::now();            \
        function;                                                                           \
        high_resolution_clock::time_point _end = std::chrono::high_resolution_clock::now(); \
        auto _runtime = duration_cast<miliseconds>(_end - _start).count();                  \
        std::cout << "[ PERFORMANCE ] Function: " << #function << std::endl;                \
        std::cout << "                Execution time: " << _runtime << "ms" << std::endl;)

#define ASSERT(condition, message) \
    MAKE_SINGLE_STATEMENT(         \
        if (!(condition))          \
            LOG_ERROR(message);)

