#pragma once

#include <orhi/common.hh>
#include <orhi/graphics/resource.hh>

namespace orhi::graphics
{

class Buffer : public Resource
{
public:
    void setData(const Eigen::Ref<const MatrixXf> &data);
    void setData(const Eigen::Ref<const MatrixXi> &data);

    void setSubData(int data, int offset);
    void setSubData(float data, int offset);
    void setSubData(const Eigen::Ref<const MatrixXf> &data, int offset, int size);

    void reserveMemory(unsigned int size);

    Buffer();
    ~Buffer();

private:
    unsigned int m_size;
};

} // namespace orhi::graphics