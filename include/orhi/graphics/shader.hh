#pragma once

#include <orhi/common.hh>
#include <orhi/graphics/resource.hh>

namespace orhi::graphics
{

enum class ShaderType
{
    VERTEX_SHADER,
    GEOMETRY_SHADER,
    FRAGMENT_SHADER
};

class Shader : public Resource
{
public:
    Shader(const std::string &source, ShaderType type);
    ~Shader();
};

} // namespace orhi