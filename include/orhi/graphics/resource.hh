#pragma once

namespace orhi::graphics
{

class Resource
{
public:
    unsigned int getHandle() const
    {
        return m_handle;
    }

protected:
    unsigned int m_handle = 0;
};

} // namespace orhi::graphics
