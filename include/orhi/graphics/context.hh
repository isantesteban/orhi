#include <orhi/common.hh>
#include <glad/glad.h>

struct GLFWwindow;

namespace orhi::graphics
{

void Initialize(GLFWwindow *window);
void StartFrame();
void EndFrame();

void CheckShader(unsigned int handle);
void CheckProgram(unsigned int handle);

static const unsigned int SIZE_VEC_2F = 2 * sizeof(float);
static const unsigned int SIZE_VEC_3F = 3 * sizeof(float);
static const unsigned int SIZE_VEC_4F = 4 * sizeof(float);

static const unsigned int SIZE_VEC_2I = 2 * sizeof(int);
static const unsigned int SIZE_VEC_3I = 3 * sizeof(int);
static const unsigned int SIZE_VEC_4I = 4 * sizeof(int);

static const unsigned int SIZE_MAT_2F = 4 * sizeof(float);
static const unsigned int SIZE_MAT_3F = 9 * sizeof(float);
static const unsigned int SIZE_MAT_4F = 16 * sizeof(float);

static const unsigned int SIZE_MAT_2I = 4 * sizeof(int);
static const unsigned int SIZE_MAT_3I = 9 * sizeof(int);
static const unsigned int SIZE_MAT_4I = 16 * sizeof(int);

static const unsigned int SIZE_POINT_LIGHT = 8 * sizeof(float);
static const unsigned int SIZE_DIRECTIONAL_LIGHT = 8 * sizeof(float);

static const unsigned int MAX_POINT_LIGHTS = 5; // TODO: pass this value to shader
static const unsigned int MAX_DIRECTIONAL_LIGHTS = 5;

} // namespace orhi::graphics
