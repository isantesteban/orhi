#pragma once

#include <orhi/common.hh>

namespace orhi
{
class Scene;
class Camera;
class Model;
} // namespace orhi

namespace orhi::graphics
{
class Buffer;

class Renderer
{
public:
    void render(std::shared_ptr<const Scene> scene, std::shared_ptr<const Camera> camera);

    Renderer();

private:
    void render(std::shared_ptr<const Model> model, std::shared_ptr<const Camera> camera) const;

    void prepareMatrixBuffer(std::shared_ptr<const Camera> camera);
    void prepareLightBuffer(std::shared_ptr<const Scene> scene, std::shared_ptr<const Camera> camera);

    std::shared_ptr<Buffer> m_matrixBuffer;
    std::shared_ptr<Buffer> m_lightBuffer;
};

} // namespace orhi::graphics