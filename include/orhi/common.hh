#pragma once

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <memory>
#include <functional>

#include <orhi/macros.hh>
#include <orhi/math/types.hh>
#include <orhi/math/color.hh>
