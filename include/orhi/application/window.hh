#pragma once

#include <orhi/common.hh>

struct GLFWwindow;
struct GLFWmonitor;

namespace orhi
{
class EventManager;

class Window
{
public:
    void open();
    void close();
    bool shouldClose() const;

    Vector2i getSize() const;
    void setSize(Vector2i size);

    int getWidth() const;
    void setWidth(int width);

    int getHeight() const;
    void setHeight(int height);

    float getScale() const;

    void swapBuffers() const;

    Window();
    ~Window();

private:
    Vector2i m_size;

    GLFWwindow *m_glfwWindow;
    GLFWmonitor *m_glfwMonitor;

    friend class EventManager;
};
} // namespace orhi