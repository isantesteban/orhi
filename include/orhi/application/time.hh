#pragma once

#include <orhi/common.hh>

namespace orhi
{

class TimeManager
{
public:
    void start();

    // Get the internal time of the application (ms)
    double getCurrentTime() const; 
    void setCurrentTime(double time); 

    // Get the difference between the running time and the current time
    double getLagTime() const; 

    // Advance the current time by an arbitrary amount of time
    void advance(double timeMs);

    // Advance the current time by a time step
    void advanceStep();
    
    void setTimeStep(double timeStepMs);
    double getTimeStep() const;

    TimeManager() : m_timeStep(1000.0f / 60.0f),
                    m_currentTime(0.0f){};

private:
    double m_timeStep; 
    double m_currentTime; 

    double getRunningTime() const; 
};

} // namespace orhi
