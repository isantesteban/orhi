#pragma once

#include <orhi/common.hh>

namespace orhi
{
class Window;
class EventManager;
class TimeManager;

namespace gui
{
class Widget;
class Style;
}

class Application : public std::enable_shared_from_this<Application>
{
public:
    void launch();

    std::shared_ptr<Window> getWindow() const;
    std::shared_ptr<TimeManager> getTimeManager() const;
    std::shared_ptr<EventManager> getEventManager() const;
    std::shared_ptr<gui::Style> getStyle() const;

    void add(std::shared_ptr<gui::Widget> widget);

    Application() : m_name("Application"){};
    Application(const std::string &name) : m_name(name){};

protected:
    virtual void onLaunch(){};
    virtual void onUpdate(){};
    virtual void onFixedUpdate(){};

private:
    std::string m_name;

    std::shared_ptr<Window> m_window;
    std::shared_ptr<TimeManager> m_timeManager;
    std::shared_ptr<EventManager> m_eventManager;
    std::shared_ptr<gui::Style> m_style;
    std::vector<std::shared_ptr<gui::Widget>> m_widgets;

    void runMainLoop();
    void update();
    void render();
};
} // namespace orhi
