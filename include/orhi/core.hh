#pragma once

#include <orhi/application/application.hh>
#include <orhi/application/events.hh>
#include <orhi/application/time.hh>
#include <orhi/application/window.hh>

#include <orhi/scene/scene.hh>
#include <orhi/scene/node.hh>
#include <orhi/scene/model.hh>

#include <orhi/scene/cameras/perspective_camera.hh>
#include <orhi/scene/cameras/orthographic_camera.hh>
#include <orhi/scene/cameras/controls/orbit_controls.hh>

#include <orhi/scene/lights/directional_light.hh>
#include <orhi/scene/lights/point_light.hh>
#include <orhi/scene/lights/spot_light.hh>

#include <orhi/scene/materials/material.hh>
#include <orhi/scene/materials/phong_material.hh>

#include <orhi/scene/meshes/mesh.hh>
#include <orhi/scene/meshes/plane_mesh.hh>
#include <orhi/scene/meshes/box_mesh.hh>
#include <orhi/scene/meshes/sphere_mesh.hh>

#include <orhi/gui/widget.hh>
#include <orhi/gui/viewport.hh>
#include <orhi/gui/panel.hh>
#include <orhi/gui/row.hh>
#include <orhi/gui/text.hh>
#include <orhi/gui/slider.hh>
#include <orhi/gui/style.hh> 
#include <orhi/gui/button.hh> 
#include <orhi/gui/checkbox.hh>
#include <orhi/gui/combo.hh> 
#include <orhi/gui/separator.hh> 
#include <orhi/gui/color_picker.hh>

#include <orhi/math/color.hh>
// #include <orhi/math/transform.hh>
// #include <orhi/math/frustum.hh>
// #include <orhi/math/box.hh>
// #include <orhi/math/sphere.hh>

#include <orhi/io/loaders.hh>
// #include <orhi/utils/controls.hh>
