#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace orhi
{
using MatrixXf = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using Matrix2f = Eigen::Matrix<float, 2, 2, Eigen::RowMajor>;
using Matrix3f = Eigen::Matrix<float, 3, 3, Eigen::RowMajor>;
using Matrix4f = Eigen::Matrix<float, 4, 4, Eigen::RowMajor>;

using MatrixXi = Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using Matrix2i = Eigen::Matrix<int, 2, 2, Eigen::RowMajor>;
using Matrix3i = Eigen::Matrix<int, 3, 3, Eigen::RowMajor>;
using Matrix4i = Eigen::Matrix<int, 4, 4, Eigen::RowMajor>;

using VectorXf = Eigen::Matrix<float, Eigen::Dynamic, 1>;
using Vector2f = Eigen::Matrix<float, 2, 1>;
using Vector3f = Eigen::Matrix<float, 3, 1>;
using Vector4f = Eigen::Matrix<float, 4, 1>;

using VectorXi = Eigen::Matrix<int, Eigen::Dynamic, 1>;
using Vector2i = Eigen::Matrix<int, 2, 1>;
using Vector3i = Eigen::Matrix<int, 3, 1>;
using Vector4i = Eigen::Matrix<int, 4, 1>;

using VectorXb = Eigen::Matrix<bool, Eigen::Dynamic, 1>;
using Vector2b = Eigen::Matrix<bool, 2, 1>;
using Vector3b = Eigen::Matrix<bool, 3, 1>;
using Vector4b = Eigen::Matrix<bool, 4, 1>;

using Quaternion = Eigen::Quaternion<float>;
using AffineTransform = Eigen::Affine3f;
using ProjectiveTransform = Eigen::Projective3f;

namespace Axis
{
    static const Vector3f X = Vector3f::UnitX();
    static const Vector3f Y = Vector3f::UnitY();
    static const Vector3f Z = Vector3f::UnitZ();
}

namespace math
{
// Mathematical constants
// -----------------------------------------

static const float E = 2.718281828459045235360287471352662497f;
static const float GOLDEN_RATIO = 1.6180339887498948482045868343656381177f;
static const float PI = 3.141592653589793238462643383279502884f;

// Convenience values
// -----------------------------------------

static const float HALF_PI = PI / 2.0f;
static const float QUARTER_PI = PI / 4.0f;
static const float DEGREE_TO_RAD = PI / 180.0f;
static const float RAD_TO_DEGREE = 180.0f / PI;
static const float EPSILON = 0.000001f;

inline float ToRadians(float angleDegrees)
{
    return angleDegrees * DEGREE_TO_RAD;
};

inline float ToDegrees(float angleRadians)
{
    return angleRadians * RAD_TO_DEGREE;
};

inline bool IsZero(float value, float epsilon = EPSILON)
{
    return abs(value) < epsilon;
};

// Quaternion AxisAngleToQuaternion(Vector3f axisAngle)
// {
//     float angle = axisAngle.norm();
//     Vector3f axis = axisAngle / angle;
//     return AxisAngleToQuaternion(axis, angle);
// }

// Quaternion AxisAngleToQuaternion(Vector3f axis, float angle)
// {
//     return Quaternion::Identity();
// }

inline Quaternion EulerAnglesToQuaternion(Vector3f eulerAngles)
{
    return Quaternion::Identity();
};

inline Vector3f QuaternionToEulerAngles(Quaternion q)
{
    return Vector3f::Identity();
};

} // namespace math

} // namespace orhi