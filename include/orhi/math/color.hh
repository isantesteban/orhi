#pragma once

#include <orhi/common.hh>

namespace orhi
{

class Color
{
public:
    Vector3f getRGB() const;
    void setRGB(Vector3f rgb);
    void setRGB(float r, float g, float b);

    Vector4f getRGBA() const;
    void setRGBA(Vector4f rgba);
    void setRGBA(float r, float g, float b, float a);

    float getAlpha() const;
    void setAlpha(float alpha);

    Color() : Color(0, 0, 0, 1){};
    Color(float r, float g, float b);
    Color(float r, float g, float b, float a);

    static const Color WHITE;
    static const Color BLACK;
    static const Color GRAY;
    static const Color RED;
    static const Color GREEN;
    static const Color BLUE;

private:
    Vector4f m_rgba;
};

Color operator*(float x, const Color &color);
Color operator*(const Color &color, float x);

} // namespace orhi