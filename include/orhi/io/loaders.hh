#pragma once

#include <orhi/common.hh>

namespace orhi
{
    class Model;
    class Mesh;
}

namespace orhi::io
{
    std::shared_ptr<Mesh> LoadMesh(const std::string &path);
}