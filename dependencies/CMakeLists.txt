add_subdirectory("stb")
add_subdirectory("imgui")
add_subdirectory("glad")
add_subdirectory("eigen")
add_subdirectory("glfw")

# GLFW3 and its dependencies
target_link_libraries(${PROJECT_NAME} PRIVATE glfw ${GLFW_LIBRARIES})



