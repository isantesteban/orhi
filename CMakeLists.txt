cmake_minimum_required(VERSION 3.13)

project(orhi_cc VERSION 0.1
                DESCRIPTION "Real-time rendering framework"
                LANGUAGES CXX)

add_library(${PROJECT_NAME} "")
add_compile_definitions(_ENABLE_EXTENDED_ALIGNED_STORAGE)

add_subdirectory("src")
add_subdirectory("dependencies")
add_subdirectory("examples")

target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
target_include_directories(${PROJECT_NAME} PUBLIC "include")
target_include_directories(${PROJECT_NAME} PRIVATE "include/framework") # TODO: remove this

find_package(OpenMP REQUIRED)
target_link_libraries(${PROJECT_NAME} PRIVATE OpenMP::OpenMP_CXX)


set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DMY_DEBUG")