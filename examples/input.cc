#include <exception>

#include <orhi/core.hh>

class InputDemo : public orhi::Application
{
public:
    void onLaunch()
    {
        auto eventManager = this->getEventManager();
        auto window = this->getWindow();
        auto timeManager = this->getTimeManager();

        eventManager->addKeyCallback(orhi::Key::A, []() {
            LOG_INFO("'A' pressed");
        });

        eventManager->addKeyCallback(orhi::Key::A, orhi::Action::RELEASE, []() {
            LOG_INFO("'A' released");
        });

        eventManager->addKeyCallback(orhi::Key::A, orhi::Action::REPEAT, []() {
            LOG_INFO("'A' repeated");
        });

        eventManager->addKeyCallback(orhi::Key::B, orhi::Action::PRESS, orhi::KeyModifier::ALT, []() {
            LOG_INFO("'Alt+B' pressed");
        });

        eventManager->addKeyCallback(orhi::Key::B, orhi::Action::PRESS, orhi::KeyModifier::SHIFT, []() {
            LOG_INFO("'Shift+B' pressed");
        });

        eventManager->addKeyCallback(orhi::Key::B, orhi::Action::PRESS, orhi::KeyModifier::ALT | orhi::KeyModifier::SHIFT, []() {
            LOG_INFO("'Shift+Alt+B' pressed");
        });

        eventManager->addKeyCallback(orhi::Key::ESCAPE, [window]() {
            LOG_INFO("'Escape' pressed");
            window->close();
        });

        eventManager->addCursorMoveCallback([](orhi::Vector2f pos) {
            LOG_INFO("Cursor position: (" << pos[0] << "," << pos[1] << ")");
        });

        eventManager->addMouseButtonCallback(orhi::MouseButton::LEFT, []() {
            LOG_INFO("Left mouse button pressed!");
        });

        eventManager->addMouseButtonCallback(orhi::MouseButton::RIGHT, orhi::Action::RELEASE, []() {
            LOG_INFO("Right mouse button released!");
        });
    }

    InputDemo() : orhi::Application("Input demo"){};
};

int main()
{
    InputDemo demo;
    demo.launch();
}