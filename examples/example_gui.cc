
#include <orhi/core.hh>


class TestDemo : public orhi::Application
{
  public:

    void onLaunch()
    {
        // GUI
        auto panel = std::make_shared<orhi::gui::Panel>("Widgets");

        panel->setRelativeHeight(1.0);
        panel->setAnchor(orhi::gui::Anchor::TOP_LEFT);

        orhi::Vector2f range;
        range << -2, 2;

        static float values[10];
        std::function<void(float)> onChange = [](float x) {LOG_INFO(x);};
        panel->add(std::make_shared<orhi::gui::Text>("Very long long long long long long long text"));
        panel->add(std::make_shared<orhi::gui::Text>("Very long long long long long long long text"));
        panel->add(std::make_shared<orhi::gui::Text>("Property sliders:"));
        panel->add(std::make_shared<orhi::gui::Slider>(&values[0], range, "Property 1", onChange));
        panel->add(std::make_shared<orhi::gui::Slider>(&values[1], range, "Property 3", onChange));
        panel->add(std::make_shared<orhi::gui::Slider>(&values[2], range, "", onChange));
        panel->add(std::make_shared<orhi::gui::Separator>());
        panel->add(std::make_shared<orhi::gui::Text>("Combos:"));
        std::vector<std::string> list = {"A", "B", "C"};
        panel->add(std::make_shared<orhi::gui::Combo>(list));
        panel->add(std::make_shared<orhi::gui::Separator>());
        panel->add(std::make_shared<orhi::gui::Text>("Checkboxes:"));
        panel->add(std::make_shared<orhi::gui::Checkbox>("Rotateeee!!", [](bool isChecked) {LOG_INFO(isChecked);}));
        panel->add(std::make_shared<orhi::gui::Checkbox>("Rotateeeeeeeee!!", [](bool isChecked) {LOG_INFO(isChecked);}));


        panel->add(std::make_shared<orhi::gui::Separator>());
        panel->add(std::make_shared<orhi::gui::Text>("Buttons:"));
        panel->add(std::make_shared<orhi::gui::Button>("Button with a veeeeeeeery looong text", []() {LOG_INFO("Button1 pressed!");}));
        auto row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::SmallButton>("Button 1", []() {LOG_INFO("Button1 pressed!");}));
        row->add(std::make_shared<orhi::gui::SmallButton>("Button 2", []() {LOG_INFO("Button1 pressed!");}));
        row->add(std::make_shared<orhi::gui::SmallButton>("Button 3", []() {LOG_INFO("Button1 pressed!");}));
        panel->add(row);

        static orhi::Color color;
        auto onColorChange = [](const orhi::Color &c){LOG_INFO(c.getRGB());};
        panel->add(std::make_shared<orhi::gui::ColorPicker>(&color));
        panel->add(std::make_shared<orhi::gui::ColorPicker>(&color, "Color", onColorChange));

        this->add(panel);

        auto controlPanel = std::make_shared<orhi::gui::Panel>();
        controlPanel->setRelativeWidth(1.0f);
        controlPanel->setAnchor(orhi::gui::Anchor::BOTTOM_LEFT);

        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Text>("Shape coefficients"));
        row->add(std::make_shared<orhi::gui::Text>("(Disabled, only available for custom subjects)", orhi::Color::GRAY));
        row->add(std::make_shared<orhi::gui::SmallButton>("Reset", []() {LOG_INFO("Button1 pressed!");}));
        controlPanel->add(row);

        static orhi::VectorXf shape(10);
        shape.setZero();

        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Slider>(&shape(0), range, "B0", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(1), range, "B1", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(2), range, "B2", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(3), range, "B3", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(4), range, "B4", onChange));
        controlPanel->add(row);

        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Slider>(&shape(5), range, "B5", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(6), range, "B6", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(7), range, "B7", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(8), range, "B8", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(9), range, "B9", onChange));
        controlPanel->add(row);

        this->add(controlPanel);

        auto text = std::make_shared<orhi::gui::Text>("Left");
        text->setAnchor(orhi::gui::Anchor::CENTER);
        
        this->add(text);
    }
   
    TestDemo() : orhi::Application("Test demo") {};

private:
    std::shared_ptr<orhi::Scene> m_scene;
};

int main()
{
    TestDemo demo;
    demo.launch();
}