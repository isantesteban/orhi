#include <exception>
#include <sstream>
#include <fstream>
#include <orhi/core.hh>

std::string LoadText(const std::string &path)
{
    std::ifstream file(path);

    ASSERT(file.good(), "Unable to read file: " + path);

    std::stringstream buffer;
    buffer << file.rdbuf();

    return buffer.str();
}

class TestDemo : public orhi::Application
{
  public:

    void onLaunch()
    {
        m_scene = std::make_shared<orhi::Scene>();
        // m_scene->setScale(0.9f);
        // m_scene->setEulerAngles(0.2, 0.8, 1.1);
        // m_scene->setPosition(0.0f, 0.5f, 0.0f);

        // SHADER    
        Eigen::MatrixXf vertices(3, 3);
        vertices << -0.5f, -0.5f, 0.0f,
                     0.5f, -0.5f, 0.0f,
                     0.0f,  0.5f, 0.0f;

        Eigen::MatrixXi faces(1,3);
        faces << 0, 1, 2;
        
        auto material = std::make_shared<orhi::PhongMaterial>();
        material->setWireframeVisible(true);
        material->setWireframeThickness(1.0f);
        //auto mesh = orhi::io::LoadMesh("/home/igor/Workspace/orhi-python/examples/bunny.obj");
        auto mesh = std::make_shared<orhi::SphereMesh>(0.95f, 64);

        auto model = std::make_shared<orhi::Model>(mesh, material);
        model->setScale(0.8f);
        // model->setPosition(1.0, -0.5, 0);
        // model->setEulerAngles(0.2, 0.8, 0.1);
        m_scene->add(model);
        LOG_INFO(model->getWorldPosition());

        m_model = model;

        m_lights = std::make_shared<orhi::Node>();
        m_scene->add(m_lights);

        mesh = std::make_shared<orhi::SphereMesh>(0.95f, 64);
        auto directionalLight = std::make_shared<orhi::DirectionalLight>();
        directionalLight->setPosition(0.0f, 1.2f, 0.0f);
        m_lights->add(directionalLight);

        auto pointLight2 = std::make_shared<orhi::PointLight>();
        pointLight2->setPosition(-1.0f, -1.0f, 0.0f);
        pointLight2->setColor(orhi::Color(1.0f, 0.3f, 0.2f));
        m_lights->add(pointLight2);

        material = std::make_shared<orhi::PhongMaterial>();
        material->setEmissive(pointLight2->getColor());
        material->setDiffuse(orhi::Color::BLACK);
        material->setSpecular(orhi::Color::BLACK);
        model = std::make_shared<orhi::Model>(mesh, material);
        model->setScale(0.2f);
        pointLight2->add(model);

        pointLight2 = std::make_shared<orhi::PointLight>();
        pointLight2->setPosition(0.7f, -0.7f, -0.7f);
        pointLight2->setColor(orhi::Color(0.2f, 0.3f, 1.0f));
        pointLight2->setRadius(pointLight2->getRadius()*0.1f);
        m_lights->add(pointLight2);

        material = std::make_shared<orhi::PhongMaterial>();
        material->setEmissive(pointLight2->getColor());
        material->setDiffuse(orhi::Color::BLACK);
        material->setSpecular(orhi::Color::BLACK);
        model = std::make_shared<orhi::Model>(mesh, material);
        model->setScale(0.1f);

        pointLight2->add(model);


        // Create m_camera
        auto camera = std::make_shared<orhi::PerspectiveCamera>();
        camera->setPosition(0.0f, 0.0f, 3.0f);
        // camera->lookAt(m_model->getPosition());
        camera->setControls(std::make_shared<orhi::OrbitControls>());
        // camera->setFov(50.0f);

        // Create a viewport and attach it to the application
        auto viewport = std::make_shared<orhi::gui::Viewport>(m_scene, camera);
        // viewport->m_backgroundColor << 0.3f, 0.8f, 1.0f;
        viewport->setAnchor(orhi::gui::Anchor::TOP_LEFT);
        viewport->setRelativeSize(0.5f, 1.0f);
        this->add(viewport);

        auto camera2 = std::make_shared<orhi::OrthographicCamera>();
        camera2->setPosition(0.0f, 0.0f, 1.5f);
        camera2->setControls(std::make_shared<orhi::OrbitControls>());
        // camera2->setScale(2.0f);

        auto viewport2 = std::make_shared<orhi::gui::Viewport>(m_scene, camera2);
        viewport2->setAnchor(orhi::gui::Anchor::TOP_RIGHT);
        viewport2->setRelativeSize(0.5f, 1.0f);
        // viewport2->m_backgroundColor << 1.0f, 0.8f, 0.3f;
        this->add(viewport2);

        // GUI
        auto controlPanel = std::make_shared<orhi::gui::Panel>();
        controlPanel->setRelativeWidth(1.0f);
        controlPanel->setAnchor(orhi::gui::Anchor::BOTTOM_LEFT);
        float em = getStyle()->getEM();
        // controlPanel->setHeight((int) (2*em + 2*2.5*em));

        auto row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Text>("Shape coefficients"));
        row->add(std::make_shared<orhi::gui::Text>("(Disabled, only available for custom subjects)", orhi::Color::GRAY));
        controlPanel->add(row);

        std::function<void(float)> onChange = [](float x) {LOG_INFO(x);};
        orhi::Vector2f range;
        range << -2, 2;
        static orhi::VectorXf shape(10);
        shape.setZero();

        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Slider>(&shape(0), range, "B0", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(1), range, "B1", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(2), range, "B2", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(3), range, "B3", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(4), range, "B4", onChange));
        controlPanel->add(row);

        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Slider>(&shape(5), range, "B5", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(6), range, "B6", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(7), range, "B7", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(8), range, "B8", onChange));
        row->add(std::make_shared<orhi::gui::Slider>(&shape(9), range, "B9", onChange));
        controlPanel->add(row);

        this->add(controlPanel);

        std::vector<std::string> values = {"A", "B", "C"};
        auto topPanel = std::make_shared<orhi::gui::Panel>();
        topPanel->setAnchor(orhi::gui::Anchor::TOP_LEFT);

        topPanel->setRelativeWidth(1.0f);
        row = std::make_shared<orhi::gui::Row>();
        row->add(std::make_shared<orhi::gui::Checkbox>("Rotate", [](bool isChecked) {LOG_INFO(isChecked);}));
        // row->add(std::make_shared<orhi::gui::Button>("Button 1", []() {LOG_INFO("Button1 pressed!");}));
        // row->add(std::make_shared<orhi::gui::SmallButton>("Button 2", []() {LOG_INFO("Button2 pressed!");}));
        // row->add(std::make_shared<orhi::gui::Text>("Combo:"));
        // row->add(std::make_shared<orhi::gui::Combo>(values));
        topPanel->add(row);

        this->add(topPanel);

        // auto text = std::make_shared<orhi::gui::Text>("AAAA");
        // text->setAnchor(orhi::gui::Anchor::CENTER);
        // this->add(text);
    }
   
    void onFixedUpdate()
    {
        auto time = getTimeManager();
        static float s = 0.01f;
        static float increment = 0.01f;


        orhi::Vector3f axis;
        // axis << 0, 1, 0;
        // m_model->rotate(axis, -0.01, orhi::Space::WORLD);
        axis << 0, 1, 0;
        m_lights->rotate(axis, 0.01f);

        orhi::Vector3f origin = orhi::Vector3f::Zero();
        origin << -1, 0, 0;
        m_model->rotateAround(origin, axis, 0.0);
        m_lights->setEulerAngles(0.0f, 0.001f* (float) time->getCurrentTime(), 0.0f);

        if (s > 1.0f)
        {
            increment = -0.01f;
        }

        if (s < 0.1f)
        {
            increment = 0.01f;
        }

        s += increment;
    }

    TestDemo() : orhi::Application("Test demo") {};

private:
    std::shared_ptr<orhi::Scene> m_scene;
    std::shared_ptr<orhi::Node> m_lights;
    std::shared_ptr<orhi::Node> m_model;
};


int main()
{
    std::shared_ptr<TestDemo> demo = std::make_shared<TestDemo>();
    demo->launch();
}