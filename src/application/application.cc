#include <orhi/application/application.hh>
#include <orhi/application/window.hh>
#include <orhi/application/events.hh>
#include <orhi/application/time.hh>
#include <orhi/graphics/context.hh>
#include <orhi/gui/widget.hh>
#include <orhi/gui/style.hh>

namespace orhi
{

void Application::launch()
{
    m_window = std::make_shared<Window>();
    m_window->open();

    m_style = std::make_shared<gui::Style>();
    m_style->setScale(m_window->getScale());

    m_eventManager = std::make_shared<EventManager>();
    m_eventManager->bind(m_window);

    m_timeManager = std::make_shared<TimeManager>();

    onLaunch();
    runMainLoop();
}

void Application::runMainLoop()
{
    LOG_DEBUG("Starting main loop");

    m_timeManager->start();

    while (!m_window->shouldClose())
    {   
        update();
        render();       
    }
}

void Application::update()
{
    m_eventManager->pollEvents();

    int numSteps = (int)floor(m_timeManager->getLagTime() / m_timeManager->getTimeStep());

    for (int i = 0; i < numSteps; i++)
    {
        onFixedUpdate();
        m_timeManager->advanceStep();
    }

    onUpdate();
}

void Application::render()
{
    graphics::StartFrame();

    for (auto const &widget : m_widgets)
    {
        widget->onPreRender();
        widget->render();
        widget->onPostRender();
    }

    graphics::EndFrame();

    m_window->swapBuffers();
}

std::shared_ptr<Window> Application::getWindow() const
{
    return m_window;
}

std::shared_ptr<TimeManager> Application::getTimeManager() const
{
    return m_timeManager;
}

std::shared_ptr<EventManager> Application::getEventManager() const
{
    return m_eventManager;
}

std::shared_ptr<gui::Style> Application::getStyle() const
{
    return m_style;
}

void Application::add(std::shared_ptr<gui::Widget> widget)
{
    m_widgets.push_back(widget);
    widget->bind(shared_from_this());
}

} // namespace orhi
