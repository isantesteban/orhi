#include <orhi/application/window.hh>
#include <orhi/graphics/context.hh>
#include <GLFW/glfw3.h>

namespace orhi
{

Window::Window()
{
    ASSERT(glfwInit(), "GLFW initialization failed");

    glfwSetErrorCallback([](int error, const char *msg) { LOG_ERROR(msg); });

    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);

#ifdef DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    m_glfwMonitor = glfwGetPrimaryMonitor();

    if (m_glfwMonitor)
    {
        const GLFWvidmode *mode = glfwGetVideoMode(m_glfwMonitor);
        m_size << (int)(0.8f * mode->width), (int)(0.8f * mode->height);
    }
    else
    {
        m_size << 1280, 720;
    }

}

Window::~Window()
{
    glfwTerminate();
}

void Window::open()
{
    m_glfwWindow = glfwCreateWindow(m_size(0), m_size(1), "Demo", nullptr, nullptr);
    ASSERT(m_glfwWindow, "Failed to create a window");
    graphics::Initialize(m_glfwWindow);
}

void Window::close()
{
    if (m_glfwWindow == NULL)
    {
        return;
    }

    glfwSetWindowShouldClose(m_glfwWindow, true);
}

bool Window::shouldClose() const
{
    return glfwWindowShouldClose(m_glfwWindow);
}

void Window::swapBuffers() const
{
    glfwSwapBuffers(m_glfwWindow);
}

Vector2i Window::getSize() const
{
    return m_size;
}

void Window::setSize(Vector2i size)
{
    m_size = size;

    if (m_glfwWindow)
    {
        glfwSetWindowSize(m_glfwWindow, m_size(0), m_size(1));
    }
}

int Window::getWidth() const
{
    return m_size(0);
}

void Window::setWidth(int width)
{
    m_size(0) = width;

    if (m_glfwWindow)
    {
        glfwSetWindowSize(m_glfwWindow, m_size(0), m_size(1));
    }
}

int Window::getHeight() const
{
    return m_size(1);
}

void Window::setHeight(int height)
{
    m_size(1) = height;

    if (m_glfwWindow)
    {
        glfwSetWindowSize(m_glfwWindow, m_size(0), m_size(1));
    }
}

float Window::getScale() const
{
    if (m_glfwMonitor)
    {
        float xscale, yscale;
        glfwGetMonitorContentScale(m_glfwMonitor, &xscale, &yscale);
        return xscale;
    }
    
    return 1.0f;
}

} // namespace orhi