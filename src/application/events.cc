#include <orhi/application/events.hh>
#include <orhi/application/window.hh>
#include <GLFW/glfw3.h>

namespace orhi
{

void EventManager::bind(std::shared_ptr<Window> window)
{
    if (window == nullptr || window->m_glfwWindow == NULL)
    {
        LOG_ERROR("Cannot bind to an unopened window");
        return;
    }

    if (m_window != nullptr)
    {
        unbind();
    }

    m_window = window;

    auto onKey = [](GLFWwindow *glfwWindow, int glfwKey, int glfwScancode, int glfwAction, int glfwMods) {
        EventManager *eventManager = (EventManager *)glfwGetWindowUserPointer(glfwWindow);

        for (auto &callback : eventManager->m_keyCallbacks)
        {
            callback(glfwKey, glfwAction, glfwMods);
        }
    };

    auto onMouseButton = [](GLFWwindow *glfwWindow, int glfwButton, int glfwAction, int glfwMods) {
        EventManager *eventManager = (EventManager *)glfwGetWindowUserPointer(glfwWindow);

        for (auto &callback : eventManager->m_mouseButtonCallbacks)
        {
            callback(glfwButton, glfwAction, glfwMods);
        }
    };

    auto onResize = [](GLFWwindow *glfwWindow, int width, int height) {
        EventManager *eventManager = (EventManager *)glfwGetWindowUserPointer(glfwWindow);

        eventManager->m_window->m_size << width, height; 

        for (auto &callback : eventManager->m_resizeCallbacks)
        {
            callback(width, height);
        }
    };

    auto onScroll = [](GLFWwindow *glfwWindow, double xoffset, double yoffset) {
        EventManager *eventManager = (EventManager *)glfwGetWindowUserPointer(glfwWindow);

        Vector2f offset;
        offset << (float) xoffset, (float) yoffset;

        for (auto &callback : eventManager->m_scrollCallbacks)
        {
            callback(offset);
        }
    };

    auto onCursorMove = [](GLFWwindow *glfwWindow, double xpos, double ypos) {
        EventManager *eventManager = (EventManager *)glfwGetWindowUserPointer(glfwWindow);

        Vector2f position;
        position << (float) xpos, (float) ypos;

        for (auto &callback : eventManager->m_cursorMoveCallbacks)
        {
            callback(position);
        }
    };

    glfwSetWindowUserPointer(window->m_glfwWindow, this);
    glfwSetKeyCallback(window->m_glfwWindow, onKey);
    glfwSetCursorPosCallback(window->m_glfwWindow, onCursorMove);
    glfwSetMouseButtonCallback(window->m_glfwWindow, onMouseButton);
    glfwSetFramebufferSizeCallback(window->m_glfwWindow, onResize);
    glfwSetScrollCallback(m_window->m_glfwWindow, onScroll);

    // Close window when 'Escape' is pressed
    addKeyCallback(Key::ESCAPE, [window]() {
        window->close();
    });
}

void EventManager::unbind()
{
    // TODO: implement
}

void EventManager::pollEvents() const
{
    glfwPollEvents();
}

void EventManager::addKeyCallback(Key key, Action action, int modifiers, std::function<void()> callback)
{
    auto callbackWrapper = [=](int glfwKey, int glfwAction, int glfwMods) {
        if (glfwKey == (int)key && glfwAction == (int)action && glfwMods == modifiers)
        {
            callback();
        }
    };

    m_keyCallbacks.push_back(callbackWrapper);
}

void EventManager::addKeyCallback(Key key, Action action, std::function<void()> callback)
{
    auto callbackWrapper = [=](int glfwKey, int glfwAction, int) {
        if (glfwKey == (int)key && glfwAction == (int)action)
        {
            callback();
        }
    };

    m_keyCallbacks.push_back(callbackWrapper);
}

void EventManager::addKeyCallback(Key key, std::function<void()> callback)
{
    addKeyCallback(key, Action::PRESS, callback);
}

void EventManager::addCursorMoveCallback(std::function<void(Vector2f)> callback)
{
    m_cursorMoveCallbacks.push_back(callback);
}

void EventManager::addMouseButtonCallback(MouseButton button, Action action, int modifiers, std::function<void()> callback)
{
    auto callbackWrapper = [=](int glfwButton, int glfwAction, int glfwMods) {
        if (glfwButton == (int)button && glfwAction == (int)action && glfwMods == modifiers)
        {
            callback();
        }
    };

    m_mouseButtonCallbacks.push_back(callbackWrapper);
}

void EventManager::addMouseButtonCallback(MouseButton button, Action action, std::function<void()> callback)
{
    auto callbackWrapper = [=](int glfwButton, int glfwAction, int) {
        if (glfwButton == (int)button && glfwAction == (int)action)
        {
            callback();
        }
    };

    m_mouseButtonCallbacks.push_back(callbackWrapper);
}

void EventManager::addMouseButtonCallback(MouseButton button, std::function<void()> callback)
{
    addMouseButtonCallback(button, Action::PRESS, callback);
}

void EventManager::addScrollCallback(std::function<void(Vector2f)> callback)
{
    m_scrollCallbacks.push_back(callback);
}


void EventManager::addResizeCallback(std::function<void(int, int)> callback)
{
    m_resizeCallbacks.push_back(callback);
}

} // namespace orhi