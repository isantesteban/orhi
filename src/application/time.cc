#include <orhi/application/time.hh>
#include <GLFW/glfw3.h>

namespace orhi
{

void TimeManager::start()
{
    glfwSetTime(0.0f);
    m_currentTime = 0.0f;
}

void TimeManager::advance(double timeMs)
{
    m_currentTime += timeMs;
}

void TimeManager::advanceStep()
{
    advance(m_timeStep);
}

void TimeManager::setTimeStep(double timeStepMs)
{
    m_timeStep = timeStepMs;
}

double TimeManager::getTimeStep() const
{
    return m_timeStep;
}

double TimeManager::getRunningTime() const
{
    return 1000.0f * glfwGetTime();
}

double TimeManager::getCurrentTime() const
{
    return m_currentTime;
}

void TimeManager::setCurrentTime(double time)
{
    glfwSetTime(time / 1000.0);
    m_currentTime = time;
}

double TimeManager::getLagTime() const
{
    return getRunningTime() - getCurrentTime();
}

} // namespace orhi