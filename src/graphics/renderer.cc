#include <orhi/graphics/renderer.hh>
#include <orhi/graphics/context.hh>
#include <orhi/graphics/buffer.hh>

#include <orhi/scene/scene.hh>
#include <orhi/scene/model.hh>
#include <orhi/scene/cameras/camera.hh>
#include <orhi/scene/lights/light.hh>
#include <orhi/scene/lights/point_light.hh>
#include <orhi/scene/lights/directional_light.hh>
#include <orhi/scene/meshes/mesh.hh>
#include <orhi/scene/materials/material.hh>

namespace orhi::graphics
{

Renderer::Renderer()
{
    int matrixBufferSize = 5 * SIZE_MAT_4F;
    m_matrixBuffer = std::make_shared<Buffer>();
    m_matrixBuffer->reserveMemory(matrixBufferSize);

    int lightBufferSize = MAX_POINT_LIGHTS * SIZE_POINT_LIGHT + MAX_DIRECTIONAL_LIGHTS * SIZE_DIRECTIONAL_LIGHT + 2 * sizeof(int);
    m_lightBuffer = std::make_shared<Buffer>();
    m_lightBuffer->reserveMemory(lightBufferSize);

    /*
    layout (std140, binding = 1) uniform Matrices
    {
        mat4 modelMatrix;
        mat4 viewMatrix;
        mat4 projectionMatrix;   
        mat4 modelViewMatrix; 
    }; 

    layout (std140, binding = 2) uniform Lights
    {
        PointLight pointLights[MAX_POINT_LIGHTS];
        DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS]
        SpotLight spotLights[MAX_SPOT_LIGHTS]
        int numPointLights;
        int numDirectionalLights;
        int numSpotLights;
    }; 
    */
}

void Renderer::render(std::shared_ptr<const Scene> scene, std::shared_ptr<const Camera> camera)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    prepareMatrixBuffer(camera);
    prepareLightBuffer(scene, camera);

    for (auto const &model : scene->getChildrenByType<Model>())
    {
        if (camera->isVisible(model))
        {
            render(model, camera);
        }
    }
}

void Renderer::render(std::shared_ptr<const Model> model, std::shared_ptr<const Camera> camera) const
{
    Matrix4f viewMatrix = camera->getWorldTransformInverse().matrix();
    Matrix4f modelMatrix = model->getWorldTransform().matrix();
    Matrix4f modelViewMatrix = viewMatrix * modelMatrix;
    Matrix4f normalMatrix = modelViewMatrix.inverse().transpose();

    m_matrixBuffer->setSubData(modelMatrix, 0, SIZE_MAT_4F);
    m_matrixBuffer->setSubData(modelViewMatrix, 3 * SIZE_MAT_4F, SIZE_MAT_4F);
    m_matrixBuffer->setSubData(normalMatrix, 4 * SIZE_MAT_4F, SIZE_MAT_4F);

    auto material = model->getMaterial();
    glUseProgram(material->getHandle());

    auto mesh = model->getMesh();
    glBindVertexArray(mesh->getHandle());

    glDrawElements(GL_TRIANGLES, 3 * mesh->getNumFaces(), GL_UNSIGNED_INT, nullptr);
}

void Renderer::prepareMatrixBuffer(std::shared_ptr<const Camera> camera)
{
    m_matrixBuffer->setSubData(camera->getWorldTransformInverse().matrix(), 1 * SIZE_MAT_4F, SIZE_MAT_4F);
    m_matrixBuffer->setSubData(camera->getProjectionMatrix(), 2 * SIZE_MAT_4F, SIZE_MAT_4F);

    glBindBufferBase(GL_UNIFORM_BUFFER, 1, m_matrixBuffer->getHandle());
}

void Renderer::prepareLightBuffer(std::shared_ptr<const Scene> scene, std::shared_ptr<const Camera> camera)
{
    std::vector<std::shared_ptr<DirectionalLight>> directionalLights = scene->getChildrenByType<DirectionalLight>();
    std::vector<std::shared_ptr<PointLight>> pointLights = scene->getChildrenByType<PointLight>();

    int numPointLights = (int) pointLights.size();
    int numDirectionalLights = (int) directionalLights.size();


    ASSERT(numDirectionalLights <= MAX_DIRECTIONAL_LIGHTS, "Number of directional lights exceeded");
    ASSERT(numPointLights <= MAX_POINT_LIGHTS, "Number of point lights exceeded");

    const int sizePointLights = MAX_POINT_LIGHTS * SIZE_POINT_LIGHT;
    const int sizeDirectionalLights = MAX_DIRECTIONAL_LIGHTS * SIZE_DIRECTIONAL_LIGHT;
    const int sizeLights = sizePointLights + sizeDirectionalLights;

    m_lightBuffer->setSubData(numPointLights, sizeLights);
    m_lightBuffer->setSubData(numDirectionalLights, sizeLights + 4);

    Matrix4f viewMatrix = camera->getWorldTransformInverse().matrix();

    // Point lights
    for (int i = 0; i < numPointLights; i++)
    {
        Vector4f position;
        position << pointLights[i]->getPosition(), 1.0f;

        // Convert light position to view space
        Matrix4f modelMatrix = pointLights[i]->getWorldTransform().matrix();
        position = viewMatrix * modelMatrix * position;

        // Prepare buffer data
        VectorXf data = VectorXf::Ones(SIZE_POINT_LIGHT / sizeof(float));
        data.segment(0,3) = position.head<3>();
        data(3) = pointLights[i]->getRadius();
        data.segment(4,3) = pointLights[i]->getColor().getRGB();
        data(7) = pointLights[i]->getIntensity();

        // Set buffer data
        m_lightBuffer->setSubData(data, i * SIZE_POINT_LIGHT, SIZE_POINT_LIGHT);
    }

    // Directional lights
    for (int i = 0; i < numDirectionalLights; i++)
    {
        Vector4f direction;
        direction << directionalLights[i]->getPosition(), 0.0f;

        // Convert light direction to view space
        Matrix4f modelMatrix = directionalLights[i]->getWorldTransform().matrix();
        direction = viewMatrix * modelMatrix * direction;
        direction.head<3>().normalize();

        // Prepare buffer data
        VectorXf data = VectorXf::Ones(SIZE_DIRECTIONAL_LIGHT / sizeof(float));
        data.segment(0,3) = direction.head<3>();
        data.segment(4,3) = directionalLights[i]->getColor().getRGB();
        data(7) = directionalLights[i]->getIntensity();

        // Set buffer data
        m_lightBuffer->setSubData(data, sizePointLights + i * SIZE_DIRECTIONAL_LIGHT, SIZE_DIRECTIONAL_LIGHT);
    }

    glBindBufferBase(GL_UNIFORM_BUFFER, 2, m_lightBuffer->getHandle());
}

} // namespace orhi