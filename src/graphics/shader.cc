#include <orhi/graphics/shader.hh>
#include <orhi/graphics/context.hh>

namespace orhi::graphics
{

unsigned int createShader(ShaderType type)
{
    switch (type)
    {
    case ShaderType::VERTEX_SHADER:
        return glCreateShader(GL_VERTEX_SHADER);
    case ShaderType::GEOMETRY_SHADER:
        return glCreateShader(GL_GEOMETRY_SHADER);
    case ShaderType::FRAGMENT_SHADER:
        return glCreateShader(GL_FRAGMENT_SHADER);
    default:
        LOG_ERROR("Unsupported shader type");
        return 0;
    }
}

Shader::Shader(const std::string &source, ShaderType type)
{
    m_handle = createShader(type);
    const char *shaderSource = source.c_str();
    glShaderSource(m_handle, 1, &shaderSource, nullptr);
    glCompileShader(m_handle);
    graphics::CheckShader(m_handle);
}

Shader::~Shader()
{
    glDeleteShader(m_handle);
}

}