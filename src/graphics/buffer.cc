#include <orhi/graphics/buffer.hh>
#include <orhi/graphics/context.hh>

namespace orhi::graphics
{

Buffer::Buffer()
{
    glCreateBuffers(1, &m_handle);
}

Buffer::~Buffer()
{
    glDeleteBuffers(1, &m_handle);
}

void Buffer::setData(const Eigen::Ref<const MatrixXf> &data)
{
    m_size = (unsigned int)data.size() * sizeof(float);
    glNamedBufferData(m_handle, m_size, data.data(), GL_STATIC_DRAW);
}

void Buffer::setData(const Eigen::Ref<const MatrixXi> &data)
{
    m_size = (unsigned int)data.size() * sizeof(float);
    glNamedBufferData(m_handle, m_size, data.data(), GL_STATIC_DRAW);
}

void Buffer::setSubData(int data, int offset)
{
    int size = sizeof(int);
    ASSERT(offset + size <= (int) m_size, "The provided range exceeds the size of the buffer");
    glNamedBufferSubData(m_handle, offset, size, &data);
}

void Buffer::setSubData(float data, int offset)
{
    int size = sizeof(float);
    ASSERT(offset + size <= (int) m_size, "The provided range exceeds the size of the buffer");
    glNamedBufferSubData(m_handle, offset, size, &data);
}

void Buffer::setSubData(const Eigen::Ref<const MatrixXf> &data, int offset, int size)
{
    ASSERT(offset + size <= (int) m_size, "The provided range exceeds the size of the buffer");
    glNamedBufferSubData(m_handle, offset, size, data.data());
}

void Buffer::reserveMemory(unsigned int size)
{
    m_size = size;
    glNamedBufferData(m_handle, m_size, NULL, GL_STATIC_DRAW);
}

} // namespace orhi
