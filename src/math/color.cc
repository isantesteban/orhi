#include <orhi/math/color.hh>

namespace orhi
{

Color::Color(float r, float g, float b)
{
    setRGBA(r, g, b, 1.0f);
}

Color::Color(float r, float g, float b, float a)
{
    setRGBA(r, g, b, a);
}

Vector3f Color::getRGB() const
{
    return m_rgba.segment(0, 3);
}

void Color::setRGB(Vector3f rgb)
{
    m_rgba.segment(0, 3) = rgb;
}

void Color::setRGB(float r, float g, float b)
{
    m_rgba.segment(0, 3) << r, g, b;
}

Vector4f Color::getRGBA() const
{
    return m_rgba;
}

void Color::setRGBA(Vector4f rgba)
{
    m_rgba = rgba;
}

void Color::setRGBA(float r, float g, float b, float a)
{
    m_rgba << r, g, b, a;
}

float Color::getAlpha() const
{
    return m_rgba(3);
}

void Color::setAlpha(float alpha)
{
    m_rgba(3) = alpha;
}

Color operator*(float x, const Color &color)
{
    Color newColor;
    newColor.setRGBA(x * color.getRGBA());
    return newColor;
}

Color operator*(const Color &color, float x)
{
    return x * color;
}

const Color Color::WHITE = Color(1.0f, 1.0f, 1.0f);
const Color Color::BLACK = Color(0.0f, 0.0f, 0.0f);
const Color Color::GRAY  = Color(0.5f, 0.5f, 0.5f);
const Color Color::RED   = Color(1.0f, 0.0f, 0.0f);
const Color Color::GREEN = Color(0.0f, 1.0f, 0.0f);
const Color Color::BLUE  = Color(0.0f, 0.0f, 1.0f);
}