#include <orhi/scene/materials/phong_material.hh>
#include <orhi/graphics/shader.hh>

#include <sstream>
#include <fstream>

namespace orhi
{

static const std::string vsSource = std::string("#version 430 core\n") + 
STRINGIFY(
    layout (location = 0) in vec3 position;
    layout (location = 1) in vec3 normal;

    layout (std140, binding = 1) uniform Matrices
    {
        mat4 modelMatrix;
        mat4 viewMatrix;
        mat4 projectionMatrix;
        mat4 modelViewMatrix; 
        mat3 normalMatrix;
    };

    out vec3 vPosition;
    out vec3 vNormal;

    void main()
    {
        vPosition = (vec4(position, 1.0) * modelViewMatrix).xyz;
        vNormal = normal * normalMatrix;
        gl_Position = vec4(vPosition, 1.0) * projectionMatrix;
    }
);


static const std::string gsSource = std::string("#version 430 core\n") + 
STRINGIFY(
    layout (triangles) in;
    layout (triangle_strip, max_vertices = 3) out;

    in vec3 vPosition[];
    in vec3 vNormal[];

    out vec3 gPosition;
    out vec3 gNormal;
    out vec3 gBarCoord;

    void main()
    {
        vec3 barCoord[3];
        barCoord[0] = vec3(1.0, 0.0, 0.0);
        barCoord[1] = vec3(0.0, 1.0, 0.0);
        barCoord[2] = vec3(0.0, 0.0, 1.0);
        
        for (int i = 0; i < 3; ++i)
        {
            gl_Position = gl_in[i].gl_Position;
            gPosition = vPosition[i];
            gNormal =  vNormal[i];
            gBarCoord = barCoord[i];
            EmitVertex();
        }
        
        EndPrimitive();
    }
);

static const std::string fsSource = std::string("#version 430 core\n") + 
                                    std::string("#define MAX_POINT_LIGHTS 5\n") +
                                    std::string("#define MAX_DIRECTIONAL_LIGHTS 5\n") + 
STRINGIFY(
    in vec3 gPosition;
    in vec3 gNormal;
    in vec3 gBarCoord;

    out vec4 fragColor;

    struct PhongMaterial 
    {
        vec3 diffuse;
        vec3 specular;
        vec3 emissive;
        float shininess;
    }; 

    uniform PhongMaterial material;
    uniform vec4 uWireframeColor;
    uniform float uWireframeThickness;

    struct PointLight    
    {
        vec3 position;  
        float radius;
        vec3 color;      
        float intensity;
    };

    struct DirectionalLight    
    {
        vec3 direction;  
        vec3 color;      
        float intensity;
    };

    layout (std140, binding = 2) uniform Lights
    {
        PointLight pointLights[MAX_POINT_LIGHTS];
        DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
        int numPointLights;
        int numDirectionalLights;
    };

    vec3 ComputePointLight(PointLight light, PhongMaterial mat, vec3 fragPosition, vec3 fragNormal, vec3 viewDir)
    {
        vec3 lightDir = normalize(light.position - fragPosition);
        float diffuseIntensity = max(dot(fragNormal, lightDir), 0.0);
        vec3 diffuseColor = light.color * (diffuseIntensity * mat.diffuse);

        vec3 reflectDir = reflect(-lightDir, fragNormal); 
        float specularIntensity = pow(max(dot(viewDir, reflectDir), 0.0), mat.shininess);
        vec3 specularColor = light.color * (specularIntensity * mat.specular); 

        // float distance = length(light.position - fragPosition);
        // float falloff = clamp(1 - pow(distance/light.radius, 4), 0.0, 1.0);
        // falloff = pow(falloff, 2) / (pow(distance, 2) + 1);

        return light.intensity * (diffuseColor + specularColor);
    }

    vec3 ComputeDirectionalLight(DirectionalLight light, PhongMaterial mat, vec3 fragNormal, vec3 viewDir)
    {
        vec3 lightDir = light.direction;
        float diffuseIntensity = max(dot(fragNormal, lightDir), 0.0);
        vec3 diffuseColor = light.color * diffuseIntensity * mat.diffuse;

        vec3 reflectDir = reflect(-lightDir, fragNormal); 
        float specularIntensity = pow(max(dot(viewDir, reflectDir), 0.0), mat.shininess);
        vec3 specularColor = light.color * specularIntensity * mat.specular; 

        return light.intensity * (diffuseColor + specularColor);
    }

    vec3 Wireframe(vec3 color)
    {
        vec3 barCoord = gBarCoord;
        barCoord.z = 1.0 - barCoord.x - barCoord.y;
	    vec3 deltas = fwidth(barCoord);
        vec3 thickness = 0.5 * deltas * uWireframeThickness;
        barCoord = smoothstep(thickness, thickness + deltas, barCoord);

	    float w = 1.0 - min(barCoord.x, min(barCoord.y, barCoord.z));
        return mix(color, uWireframeColor.rgb, uWireframeColor.a * w);
    }

    void main()
    {
        vec3 normal = normalize(gNormal);
        vec3 viewDir = normalize(-gPosition);
        vec3 color = material.emissive;
    
        for (int i = 0; i < numPointLights; i++)
        {
            color += ComputePointLight(pointLights[i], material, gPosition, normal, viewDir);
        }

        for (int i = 0; i < numDirectionalLights; i++)
        {
            color += ComputeDirectionalLight(directionalLights[i], material, normal, viewDir);
        }

        color = Wireframe(color);
        fragColor = clamp(vec4(color, 1.0), 0.0, 1.0);
    }
);

PhongMaterial::PhongMaterial()
{
    m_vertexShader = std::make_shared<graphics::Shader>(vsSource, graphics::ShaderType::VERTEX_SHADER);
    m_geometryShader = std::make_shared<graphics::Shader>(gsSource, graphics::ShaderType::GEOMETRY_SHADER);
    m_fragmentShader = std::make_shared<graphics::Shader>(fsSource, graphics::ShaderType::FRAGMENT_SHADER);

    linkShaders();

    setDiffuse(0.9f * Color::WHITE);
    setSpecular(0.2f * Color::WHITE);
    setEmissive(Color::BLACK);
    setShininess(30.0f);

    m_wireframeColor = Color::BLACK;
    m_wireframeColor.setAlpha(0.5);
    setWireframeVisible(false);
    setWireframeThickness(1.0);
}

Color PhongMaterial::getDiffuse() const
{
    return m_diffuse;
}

void PhongMaterial::setDiffuse(Color diffuse)
{
    m_diffuse = diffuse;
    set("material.diffuse", m_diffuse);
}

Color PhongMaterial::getSpecular() const
{
    return m_specular;
}

void PhongMaterial::setSpecular(Color specular)
{
    m_specular = specular;
    set("material.specular", m_specular);
}

Color PhongMaterial::getEmissive() const
{
    return m_emissive;
}

void PhongMaterial::setEmissive(Color emissive)
{
    m_emissive = emissive;
    set("material.emissive", m_emissive);
}

float PhongMaterial::getShininess() const
{
    return m_shininess;
}

void PhongMaterial::setShininess(float shininess)
{
    m_shininess = shininess;
    set("material.shininess", m_shininess);
}

bool PhongMaterial::isWireframeVisible()
{
    return m_wireframeVisible;
}

void PhongMaterial::setWireframeVisible(bool value)
{
    m_wireframeVisible = value;

    if (m_wireframeVisible)
    {
        set("uWireframeColor", m_wireframeColor.getRGBA());
    }
    else
    {
        Vector4f color = Vector4f::Zero();
        set("uWireframeColor", color);
    }
}

void PhongMaterial::setWireframeColor(Color color)
{
    m_wireframeColor = color;

    if (m_wireframeVisible)
    {
        set("uWireframeColor", m_wireframeColor.getRGBA());
    }
}

Color PhongMaterial::getWireframeColor() const
{
    return m_wireframeColor;
}

void PhongMaterial::setWireframeThickness(float thickness)
{
    m_wireframeThickness = thickness;
    set("uWireframeThickness", m_wireframeThickness);
}

float PhongMaterial::getWireframeThickness() const
{
    return m_wireframeThickness;
}

} // namespace orhi