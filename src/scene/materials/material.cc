#include <orhi/scene/materials/material.hh>
#include <orhi/graphics/context.hh>
#include <orhi/graphics/shader.hh>

namespace orhi
{

Material::Material()
{
    m_handle = glCreateProgram();
}

Material::Material(const std::string &vertexShader,
                   const std::string &fragmentShader) : Material()
{
    m_vertexShader = std::make_shared<graphics::Shader>(vertexShader, graphics::ShaderType::VERTEX_SHADER);
    m_fragmentShader = std::make_shared<graphics::Shader>(fragmentShader, graphics::ShaderType::FRAGMENT_SHADER);

    linkShaders();
}

Material::Material(const std::string &vertexShader,
                   const std::string &geometryShader,
                   const std::string &fragmentShader) : Material()
{
    m_vertexShader = std::make_shared<graphics::Shader>(vertexShader, graphics::ShaderType::VERTEX_SHADER);
    m_geometryShader = std::make_shared<graphics::Shader>(geometryShader, graphics::ShaderType::GEOMETRY_SHADER);
    m_fragmentShader = std::make_shared<graphics::Shader>(fragmentShader, graphics::ShaderType::FRAGMENT_SHADER);

    linkShaders();
}

Material::Material(std::shared_ptr<graphics::Shader> vertexShader,
                   std::shared_ptr<graphics::Shader> fragmentShader) : Material()
{
    m_vertexShader = vertexShader;
    m_fragmentShader = fragmentShader;

    linkShaders();
}

Material::Material(std::shared_ptr<graphics::Shader> vertexShader,
                   std::shared_ptr<graphics::Shader> geometryShader,
                   std::shared_ptr<graphics::Shader> fragmentShader) : Material()
{
    m_vertexShader = vertexShader;
    m_geometryShader = geometryShader;
    m_fragmentShader = fragmentShader;

    linkShaders();
}

Material::~Material()
{
    glDeleteProgram(m_handle);
}

void Material::linkShaders()
{
    glAttachShader(m_handle, m_vertexShader->getHandle());
    glAttachShader(m_handle, m_fragmentShader->getHandle());

    if (m_geometryShader)
    {
        glAttachShader(m_handle, m_geometryShader->getHandle());
    }

    glLinkProgram(m_handle);
    graphics::CheckProgram(m_handle);
    getUniforms();
}

void Material::getUniforms()
{
    GLint numUniforms;
    glGetProgramiv(m_handle, GL_ACTIVE_UNIFORMS, &numUniforms);

    for (int i = 0; i < numUniforms; i++)
    {
        const GLsizei bufferSize = 128;
        GLchar name[bufferSize];
        GLsizei nameLength;
        GLint size; 
        GLenum type; 

        glGetActiveUniform(m_handle, (GLuint)i, bufferSize, &nameLength, &size, &type, name);

        GLint location = glGetUniformLocation(m_handle, name);

        if (location >= 0)
        {
            m_uniformLocations.insert(std::pair<std::string,int>(std::string(name), location));
            LOG_DEBUG("Shader uniform found: " << name << " (location = " << location << ")");
        }
    }
}

int Material::getUniformLocation(const std::string &key) const
{
    auto it = m_uniformLocations.find(key);

    if (it != m_uniformLocations.end())
    {
        return it->second;
    }

    LOG_WARNING("The shader does not contain uniform '" << key << "'");

    return -1;
}

void Material::set(const std::string &key, float value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniform1f(m_handle, location, value);
    }
}

void Material::set(const std::string &key, const Vector3f &value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniform3fv(m_handle, location, 1, value.data());
    }
}

void Material::set(const std::string &key, const Vector4f &value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniform4fv(m_handle, location, 1, value.data());
    }
}

void Material::set(const std::string &key, const Matrix3f &value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniformMatrix3fv(m_handle, location, 1, GL_FALSE, value.data());
    }
}

void Material::set(const std::string &key, const Matrix4f &value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniformMatrix4fv(m_handle, location, 1, GL_FALSE, value.data());
    }
}

void Material::set(const std::string &key, const Color &value) const
{
    int location = getUniformLocation(key);

    if (location >= 0)
    {
        glProgramUniform3fv(m_handle, location, 1, value.getRGB().data());
    }
}

} // namespace orhi