#include <orhi/scene/lights/light.hh>

namespace orhi
{

float Light::getIntensity() const
{
    return m_intensity;
}

void Light::setIntensity(float intensity)
{
    m_intensity = intensity;
}

Color Light::getColor() const
{
    return m_color;
}

void Light::setColor(Color color)
{
    m_color = color;
}

} // namespace orhi