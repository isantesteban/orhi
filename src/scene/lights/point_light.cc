#include <orhi/scene/lights/point_light.hh>

namespace orhi
{

float PointLight::getRadius() const
{
    return m_radius;
}

void PointLight::setRadius(float radius)
{
    m_radius = radius;
}

}