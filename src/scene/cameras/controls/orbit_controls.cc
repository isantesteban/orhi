#include <orhi/scene/cameras/controls/orbit_controls.hh>
#include <orhi/scene/cameras/camera.hh>
#include <orhi/scene/cameras/orthographic_camera.hh>
#include <orhi/application/events.hh>

namespace orhi
{

void OrbitControls::bind(std::shared_ptr<Camera> camera, std::shared_ptr<EventManager> eventManager)
{
    eventManager->addMouseButtonCallback(MouseButton::MIDDLE, Action::PRESS, [this](){
        m_orbitActivated = true;
    });

    eventManager->addMouseButtonCallback(MouseButton::MIDDLE, Action::RELEASE, [this](){
        m_orbitActivated = false;
    });

    // eventManager->addMouseButtonCallback(MouseButton::RIGHT, Action::PRESS, [this](){
    //     m_truckActivated = true;
    // });

    // eventManager->addMouseButtonCallback(MouseButton::RIGHT, Action::RELEASE, [this](){
    //     m_truckActivated = false;
    // });

    eventManager->addCursorMoveCallback([camera, this](Vector2f position) {
        Vector2f offset = m_cursorPosition - position;

        if (m_truckActivated)
        {
            truck(camera, offset);
        }
        else if (m_orbitActivated)
        {
            orbit(camera, offset);
        }

        m_cursorPosition = position;
    });

    eventManager->addScrollCallback([camera, this](Vector2f offset) {

        std::shared_ptr<OrthographicCamera> orthographicCamera = std::dynamic_pointer_cast<OrthographicCamera>(camera);
        if (orthographicCamera != nullptr)
        {
            scale(orthographicCamera, offset);
        }
        else
        {
            dolly(camera, offset);
        }
    });
}

void OrbitControls::truck(std::shared_ptr<Camera> camera, Vector2f offset)
{
    Vector3f translation = Vector3f::Zero();
    translation[0] = offset[0];
    translation[1] = -offset[1];
    translation *= m_truckSpeed;

    AffineTransform transform = camera->getWorldTransform();
    translation = transform.rotation() * translation;

    camera->translate(translation);

    // Update orbit center
    m_orbitCenter += translation;
}

void OrbitControls::orbit(std::shared_ptr<Camera> camera, Vector2f offset)
{
    AffineTransform transform = camera->getWorldTransform();

    Vector3f axis;
    float angle;
    
    axis = Axis::Y;
    axis = transform.rotation() * axis;
    angle = m_orbitSpeed * offset[0];
    camera->rotateAround(m_orbitCenter, axis, angle);

    // axis = Axis::X;
    // axis = transform.rotation() * axis;
    // angle = m_orbitSpeed * offset[1];
    // camera->rotateAround(m_orbitCenter, axis, angle);
}

void OrbitControls::dolly(std::shared_ptr<Camera> camera, Vector2f scrollOffset)
{
    Vector3f translation = Vector3f::Zero();
    translation[2] = -scrollOffset[1];
    translation[2] *= m_dollySpeed;

    AffineTransform transform = camera->getWorldTransform();
    translation = transform.rotation() * translation;

    camera->translate(translation); 
}

void OrbitControls::scale(std::shared_ptr<OrthographicCamera> camera, Vector2f scrollOffset)
{
    float scale = camera->getScale();
    scale += m_dollySpeed*scrollOffset[1];
    
    if (scale > 0.0)
    {
        camera->setScale(scale);
    }
}

}