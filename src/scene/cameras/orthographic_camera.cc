#include <orhi/scene/cameras/orthographic_camera.hh>

namespace orhi
{

OrthographicCamera::OrthographicCamera()
{
    m_left = -1.0f;
	m_right = 1.0f;
	m_bottom = -1.0f;
	m_top = 1.0f;
    m_scale = 1.0f;

    updateProjectionMatrix();
}

float OrthographicCamera::getScale() const
{
    return m_scale;
}

void OrthographicCamera::setScale(float scale)
{
    m_scale = scale;
    updateProjectionMatrix();
}

void OrthographicCamera::updateProjectionMatrix()
{
    float w = m_scale / (m_right - m_left);
    float h = m_scale / (m_top - m_bottom);
    float p = 1.0f / (m_far - m_near);

    h = h * m_aspectRatio;

    float x = (m_right + m_left) * w;
    float y = (m_top + m_bottom) * h;
    float z = (m_far + m_near) * p;

    m_projectionMatrix.setZero();
    m_projectionMatrix(0, 0) = 2 * w;
    m_projectionMatrix(0, 3) = -x;
    m_projectionMatrix(1, 1) = 2 * h;
    m_projectionMatrix(1, 3) = -y;
    m_projectionMatrix(2, 2) = -2 * p;
    m_projectionMatrix(2, 3) = -z;
    m_projectionMatrix(3, 3) = 1;
}

bool OrthographicCamera::isVisible(std::shared_ptr<const Node> node) const
{
    return true; // TODO: implement
}

}