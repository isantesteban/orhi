#include <orhi/scene/cameras/camera.hh>

namespace orhi
{

Matrix4f Camera::getProjectionMatrix() const
{
    return m_projectionMatrix;
}

float Camera::getAspectRatio() const
{
    return m_aspectRatio;
}

void Camera::setAspectRatio(float aspectRatio)
{
    m_aspectRatio = aspectRatio;
    updateProjectionMatrix();
}

float Camera::getNear() const
{
    return m_near;
}

void Camera::setNear(float near)
{
    m_near = near;
    updateProjectionMatrix();
}

float Camera::getFar() const
{
    return m_far;
}

void Camera::setFar(float far)
{
    m_far = far;
    updateProjectionMatrix();
}

std::shared_ptr<CameraControls> Camera::getControls() const
{
    return m_controls;
}

void Camera::setControls(std::shared_ptr<CameraControls> controls)
{
    m_controls = controls;
}

} // namespace orhi