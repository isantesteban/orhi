#include <orhi/scene/cameras/perspective_camera.hh>

namespace orhi
{

PerspectiveCamera::PerspectiveCamera()
{
    m_fov = 50.0f;
    m_near = 0.1f;
    m_far = 100.0f;
    m_aspectRatio = 1.0f;

    updateProjectionMatrix();
}

void PerspectiveCamera::updateProjectionMatrix()
{
    float fovRadians = math::ToRadians(m_fov);
    float invTanHalfFovy = 1.0f / tan(fovRadians / 2.0f);

    m_projectionMatrix.setZero();
    m_projectionMatrix(0, 0) = invTanHalfFovy / m_aspectRatio;
    m_projectionMatrix(1, 1) = invTanHalfFovy;
    m_projectionMatrix(2, 2) = -(m_far + m_near) / (m_far - m_near);
    m_projectionMatrix(2, 3) = -(2.0f * m_far * m_near) / (m_far - m_near);
    m_projectionMatrix(3, 2) = -1.0f;
}

bool PerspectiveCamera::isVisible(std::shared_ptr<const Node> node) const
{
    return true; // TODO: implement
}

float PerspectiveCamera::getFov() const
{
    return m_fov;
}

void PerspectiveCamera::setFov(float fov) 
{
    m_fov = fov;
    updateProjectionMatrix();
}

} // namespace orhi