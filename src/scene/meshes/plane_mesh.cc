#include <orhi/scene/meshes/plane_mesh.hh>

namespace orhi
{

PlaneMesh::PlaneMesh(float width, float height, int segments)
{
    if (segments < 1)
    {
        LOG_WARNING("The number of segments has to be greater than 0");
        segments = 1;
    }

    int numFaces = 2 * segments * segments;
    int numVertices = (segments + 1) * (segments + 1);

    MatrixXi faces(numFaces, 3);
    MatrixXf vertices(numVertices, 3);
    MatrixXf normals(numVertices, 3);
    MatrixXf uvs(numVertices, 2);

    float widthHalf = width / 2.0f;
    float heightHalf = height / 2.0f;

    float segmentWidth = width / segments;
    float segmentHeight = height / segments;

    // Create vertices
    for (int i = 0; i < (segments + 1); i++)
    {
        float y = i * segmentHeight - heightHalf;

        for (int j = 0; j < (segments + 1); j++)
        {
            float x = j * segmentWidth - widthHalf;

            int vertexIndex = i * (segments + 1) + j;

            vertices(vertexIndex, 0) = x;
            vertices(vertexIndex, 1) = y;
            vertices(vertexIndex, 2) = 0.0f;

            normals(vertexIndex, 0) = 0.0f;
            normals(vertexIndex, 1) = 0.0f;
            normals(vertexIndex, 2) = 1.0f;

            uvs(vertexIndex, 0) = j / (float)segments;
            uvs(vertexIndex, 1) = 1.0f - i / (float)segments;
        }
    }

    // Create faces
    for (int i = 0; i < segments; i++)
    {
        for (int j = 0; j < segments; j++)
        {
            int v0 = j + (segments + 1) * i;
            int v1 = j + (segments + 1) * (i + 1);
            int v2 = (j + 1) + (segments + 1) * (i + 1);
            int v3 = (j + 1) + (segments + 1) * i;

            int faceIndex = 2 * (i * segments + j);

            faces(faceIndex, 0) = v0;
            faces(faceIndex, 1) = v1;
            faces(faceIndex, 2) = v3;

            faces(faceIndex + 1, 0) = v1;
            faces(faceIndex + 1, 1) = v2;
            faces(faceIndex + 1, 2) = v3;
        }
    }

    setFaces(faces);
    setVertices(vertices);
    setNormals(normals);
    setTextureCoordinates(uvs);
}

} // namespace orhi