#include <orhi/scene/meshes/sphere_mesh.hh>

namespace orhi
{

SphereMesh::SphereMesh(float radius, int segments)
{
    if (segments < 3)
    {
        LOG_WARNING("The number of segments has to be greater than 2");
        segments = 3;
    }

    int numFaces = 2 * (segments * (segments - 1));
    int numVertices = (segments + 1) * (segments + 1);

    MatrixXi faces(numFaces, 3);
    MatrixXf vertices(numVertices, 3);
    MatrixXf normals(numVertices, 3);
    // MatrixXf uvs(numVertices, 2);

	float phiStart = 0.0f;
	float phiLength = math::PI * 2;
 
	float thetaStart = 0.0f;
	float thetaLength = math::PI;
	float thetaEnd = math::PI;

    // Create vertices
    for (int i = 0; i <= segments; i++)
    {
        float v = i / (float) segments;

        for (int j = 0; j <= segments; j++)
        {
            float u = j / (float) segments;

            float x = -radius * cosf(phiStart + u * phiLength) * sinf(thetaStart + v * thetaLength);
            float y = radius * cosf(thetaStart + v * thetaLength);
            float z = radius * sinf(phiStart + u * phiLength) * sinf(thetaStart + v * thetaLength);

            int vertexIndex = i * (segments + 1) + j;

            vertices(vertexIndex, 0) = x;
            vertices(vertexIndex, 1) = y;
            vertices(vertexIndex, 2) = z;

            normals(vertexIndex, 0) = x;
            normals(vertexIndex, 1) = y;
            normals(vertexIndex, 2) = z;

            normals.row(vertexIndex).normalize();
        }
    }

    // Create faces
    int faceIndex = 0;
    for (int i = 0; i < segments; i++)
    {
        for (int j = 0; j < segments; j++)
        {
            int v0 = i * (segments + 1) + (j + 1);
            int v1 = i * (segments + 1) + j;
            int v2 = (i + 1) * (segments + 1) + j;
            int v3 = (i + 1) * (segments + 1) + (j + 1);

            if (i != 0 || thetaStart > 0)
            {
                faces(faceIndex, 0) = v0;
                faces(faceIndex, 1) = v1;
                faces(faceIndex, 2) = v3;
                faceIndex++;
            }

            if (i != segments - 1 || thetaEnd < math::PI)
            {
                faces(faceIndex, 0) = v1;
                faces(faceIndex, 1) = v2;
                faces(faceIndex, 2) = v3;
                faceIndex++;
            }
        }
    }

    setFaces(faces);
    setVertices(vertices);
    setNormals(normals);
    // setTextureCoordinates(uvs);
}

} // namespace orhi
