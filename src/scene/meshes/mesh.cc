#include <orhi/scene/meshes/mesh.hh>
#include <orhi/graphics/context.hh>
#include <orhi/graphics/buffer.hh>

namespace orhi
{

Mesh::Mesh()
{
    glCreateVertexArrays(1, &m_handle);
}

Mesh::Mesh(const Eigen::Ref<const MatrixXf> &vertices, const Eigen::Ref<const MatrixXi> &faces)
{
    glCreateVertexArrays(1, &m_handle);

    setVertices(vertices);
    setFaces(faces);
}

Mesh::~Mesh()
{
    glDeleteVertexArrays(1, &m_handle);
}

void Mesh::setFaces(const Eigen::Ref<const MatrixXi> &faces)
{
    ASSERT(faces.cols() == 3, "Unsupported primitive type (only triangles allowed)");

    m_faces = faces;
    auto buffer = getBuffer(MeshBuffer::FACES);
    buffer->setData(m_faces);
}

MatrixXi &Mesh::getFaces()
{
    return m_faces;
}

int Mesh::getNumFaces() const
{
    return (int) m_faces.rows();
}

void Mesh::setVertices(const Eigen::Ref<const MatrixXf> &vertices)
{
    m_vertices = vertices;
    auto buffer = getBuffer(MeshBuffer::VERTEX_POSITIONS);
    buffer->setData(m_vertices);
}

MatrixXf &Mesh::getVertices()
{
    return m_vertices;
}

int Mesh::getNumVertices() const
{
    return (int) m_vertices.rows();
}

void Mesh::setNormals(const Eigen::Ref<const MatrixXf> &normals)
{
    m_normals = normals;
    auto buffer = getBuffer(MeshBuffer::VERTEX_NORMALS);
    buffer->setData(m_normals);
}

MatrixXf &Mesh::getNormals()
{
    return m_normals;
}

void Mesh::setColors(const Eigen::Ref<const MatrixXf> &colors)
{
    m_colors = colors;
    auto buffer = getBuffer(MeshBuffer::VERTEX_COLORS);
    buffer->setData(m_colors);
}

MatrixXf &Mesh::getColors()
{
    return m_colors;
}

void Mesh::setTextureCoordinates(const Eigen::Ref<const MatrixXf> &uvs)
{
    m_uvs = uvs;
    auto buffer = getBuffer(MeshBuffer::VERTEX_TEXTURE_COORDINATES);
    buffer->setData(m_uvs);
}

MatrixXf &Mesh::getTextureCoordinates()
{
    return m_uvs;
}

void Mesh::computeNormals()
{
    if (m_normals.rows() != m_vertices.rows())
    {
        m_normals.resize(m_vertices.rows(), 3);
    }

    m_normals.setZero();

    for (int i = 0; i < m_faces.rows(); i++)
    {
        const int ia = m_faces(i, 0);
        const int ib = m_faces(i, 1);
        const int ic = m_faces(i, 2);

        const Vector3f e1 = m_vertices.row(ic) - m_vertices.row(ib);
        const Vector3f e2 = m_vertices.row(ia) - m_vertices.row(ib);
        const Vector3f no = e1.cross(e2);

        m_normals.row(ia) += no;
        m_normals.row(ib) += no;
        m_normals.row(ic) += no;
    }

    // Normalize
    for (int i = 0; i < m_vertices.rows(); i++)
    {
        m_normals.row(i).normalize();
    }

    setNormals(m_normals);
}

std::shared_ptr<graphics::Buffer> Mesh::getBuffer(MeshBuffer key)
{
    auto it = m_buffers.find(key);

    if (it != m_buffers.end())
    {
        return it->second;
    }

    auto buffer = std::make_shared<graphics::Buffer>();

    if (key == MeshBuffer::FACES)
    {
        glVertexArrayElementBuffer(m_handle, buffer->getHandle());
    }
    else
    {
        unsigned int size = (key == MeshBuffer::VERTEX_TEXTURE_COORDINATES) ? 2 : 3;
        unsigned int attribute = (unsigned int) key;
        unsigned int bindingIndex = attribute; 
        glVertexArrayVertexBuffer(m_handle, bindingIndex, buffer->getHandle(), 0, size * sizeof(float));
        glEnableVertexArrayAttrib(m_handle, attribute);
        glVertexArrayAttribFormat(m_handle, attribute, size, GL_FLOAT, GL_FALSE, 0);
        glVertexArrayAttribBinding(m_handle, attribute, bindingIndex);
    }

    m_buffers.insert({key, buffer});
        
    return buffer;
}

}