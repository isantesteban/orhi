#include <orhi/scene/model.hh>
#include <orhi/scene/materials/material.hh>
#include <orhi/scene/meshes/mesh.hh>

namespace orhi
{

std::shared_ptr<Mesh> Model::getMesh() const
{
    return m_mesh;
}

void Model::setMesh(std::shared_ptr<Mesh> mesh)
{
    m_mesh = mesh;
}

std::shared_ptr<Material> Model::getMaterial() const
{
    return m_material;
}

void Model::setMaterial(std::shared_ptr<Material> material)
{
    m_material = material;
}

} // namespace orhi