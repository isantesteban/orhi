#include <orhi/scene/node.hh>

namespace orhi
{

std::shared_ptr<Node> Node::getParent() const
{
    return m_parent;
}

void Node::setParent(std::shared_ptr<Node> parent)
{
    if (m_parent != nullptr)
    {
        m_parent->remove(shared_from_this());
    }

    m_parent = parent;
    updateWorldTransform();
}

void Node::add(std::shared_ptr<Node> child)
{
    m_children.push_back(child);
    child->setParent(this->shared_from_this());
    child->updateWorldTransform();
}

void Node::remove(std::shared_ptr<Node> child)
{
    // TODO
}

std::vector<std::shared_ptr<Node>> Node::getChildren() const
{
    return m_children;
}

void Node::composeLocalTransform()
{
    m_localTransform.setIdentity();
    m_localTransform = Eigen::Translation3f(m_localPosition) * Eigen::Scaling(m_localScale) * m_localRotation * m_localTransform;
    m_localTransformInverse = m_localTransform.inverse();
}

void Node::composeWorldTransform()
{
    m_worldTransform.setIdentity();
    m_worldTransform = Eigen::Translation3f(m_worldPosition) * Eigen::Scaling(m_worldScale) * m_worldRotation * m_worldTransform;
    m_worldTransformInverse = m_worldTransform.inverse();
}

void Node::updateLocalTransform()
{
    if (m_parent != nullptr)
    {
        m_localTransform = m_worldTransform;
        m_localTransformInverse = m_parent->getWorldTransformInverse() * m_worldTransform;
    }
    else
    {
        m_localTransform = m_worldTransform;
        m_localTransformInverse = m_worldTransformInverse;
    }

    decomposeLocalTransform();
}

void Node::updateWorldTransform()
{
    if (m_parent == nullptr)
    {
        m_worldTransform = m_localTransform;
        m_worldTransformInverse = m_localTransformInverse;
    }
    else
    {
        m_worldTransform = m_parent->getWorldTransform() * m_localTransform;
        m_worldTransformInverse = m_worldTransform.inverse();
    }

    decomposeWorldTransform();

    for (auto const &child : m_children)
    {
        child->updateWorldTransform();
    }
}

AffineTransform Node::getWorldTransform() const
{
    return m_worldTransform;
}

AffineTransform Node::getLocalTransform() const
{
    return m_localTransform;
}

AffineTransform Node::getWorldTransformInverse() const
{
    return m_worldTransformInverse;
}

AffineTransform Node::getLocalTransformInverse() const
{
    return m_localTransformInverse;
}

Vector3f Node::getPosition() const
{
    return m_localPosition;
}

void Node::setPosition(Vector3f position)
{
    m_localPosition = position;
    composeLocalTransform();
    updateWorldTransform();
}

void Node::setPosition(float x, float y, float z)
{
    m_localPosition << x, y, z;
    composeLocalTransform();
    updateWorldTransform();
}

Vector3f Node::getWorldPosition() const
{
    return m_worldPosition;
}

Vector3f Node::getScale() const
{
    return m_localScale;
}

void Node::setScale(Vector3f scale)
{
    m_localScale = scale;
    composeLocalTransform();
    updateWorldTransform();
}

void Node::setScale(float x, float y, float z)
{
    m_localScale << x, y, z;
    composeLocalTransform();
    updateWorldTransform();
}

void Node::setScale(float scale)
{
    m_localScale << scale, scale, scale;
    composeLocalTransform();
    updateWorldTransform();
}

Vector3f Node::getEulerAngles() const
{
    return math::QuaternionToEulerAngles(m_localRotation);
}

void Node::setEulerAngles(Vector3f eulerAngles)
{
    auto rotX = Eigen::AngleAxisf(eulerAngles(0), Vector3f::UnitX());
    auto rotY = Eigen::AngleAxisf(eulerAngles(1), Vector3f::UnitY());
    auto rotZ = Eigen::AngleAxisf(eulerAngles(2), Vector3f::UnitZ());

    m_localRotation = rotZ * rotY * rotX;

    composeLocalTransform();
    updateWorldTransform();
}

void Node::setEulerAngles(float x, float y, float z)
{
    Vector3f eulerAngles;
    eulerAngles << x, y, z;
    setEulerAngles(eulerAngles);
}

void Node::translate(Vector3f translation, Space space)
{
    switch (space)
    {
    case Space::LOCAL:
        translateLocalSpace(translation);
        break;
    case Space::WORLD:
        translateWorldSpace(translation);
        break;
    }
}

void Node::translate(float x, float y, float z, Space space)
{
    Vector3f translation;
    translation << x, y, z;
    translate(translation, space);
}

void Node::translateLocalSpace(Vector3f translation)
{
    m_localPosition += translation;
    composeLocalTransform();
    updateWorldTransform();
}

void Node::translateWorldSpace(Vector3f translation)
{
    Vector3f translationLocal;

    if (m_parent != nullptr)
    {
        translationLocal = (m_parent->getWorldTransformInverse() * translation.homogeneous()).segment(0, 3);
    }
    else
    {
        translationLocal = translation;
    }

    translateLocalSpace(translationLocal);
}

void Node::rotate(Vector3f axis, float angle, Space space)
{
    switch (space)
    {
    case Space::LOCAL:
        rotateLocalSpace(axis, angle);
        break;
    case Space::WORLD:
        rotateWorldSpace(axis, angle);
        break;
    }
}

void Node::rotateLocalSpace(Vector3f axis, float angle)
{
    m_localRotation = Eigen::AngleAxisf(angle, axis) * m_localRotation;
    composeLocalTransform();
    updateWorldTransform();
}

void Node::rotateWorldSpace(Vector3f axis, float angle)
{
    Vector3f axisLocal;

    if (m_parent != nullptr)
    {
        Vector4f axisHomogeneous;
        axisHomogeneous << axis, 0;
        axisLocal = (m_parent->getWorldTransformInverse() * axisHomogeneous).segment(0, 3);
    }
    else
    {
        axisLocal = axis;
    }
    
    rotateLocalSpace(axisLocal, angle);
}

void Node::rotateAround(Vector3f point, Vector3f axis, float angle)
{
    AffineTransform transform = Eigen::Translation3f(point) * Eigen::AngleAxisf(angle, axis) * Eigen::Translation3f(-point);
    setWorldTransform(transform * m_worldTransform);
}

void Node::lookAt(Vector3f point)
{

}

void Node::setLocalTransform(AffineTransform transform)
{
    m_localTransform = transform;
    m_localTransformInverse = m_localTransform.inverse();
    decomposeLocalTransform();
    updateWorldTransform();
}

void Node::setWorldTransform(AffineTransform transform)
{
    m_worldTransform = transform;
    m_worldTransformInverse = m_worldTransform.inverse();
    decomposeWorldTransform();
    updateLocalTransform();

    for (auto const &child : m_children)
    {
        child->updateWorldTransform();
    }
}

void Node::decomposeWorldTransform()
{
    m_worldPosition << m_worldTransform(0, 3), m_worldTransform(1, 3), m_worldTransform(2, 3);
}

void Node::decomposeLocalTransform()
{
    // m_localPosition << m_localTransform(0, 3), m_localTransform(1, 3), m_localTransform(2, 3);

    Matrix4f localMatrix = m_localTransform.matrix();

    Matrix3f rotationMatrix = localMatrix.block(0, 0, 3, 3);
    m_localScale = rotationMatrix.colwise().norm();
    m_localPosition = localMatrix.col(3).segment(0,3);

    // If determinant is negative, invert the sign of the scale in one of the axes
    if (localMatrix.determinant() < 0)
    {
        m_localScale = -m_localScale;
    }

    rotationMatrix.col(0) /= m_localScale(0);
    rotationMatrix.col(1) /= m_localScale(1);
    rotationMatrix.col(2) /= m_localScale(2);

    m_localRotation = rotationMatrix;
}

} // namespace orhi