#include <orhi/gui/checkbox.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

Checkbox::Checkbox(const std::string &label, std::function<void(bool)> onChange) : m_label(label),
                                                                                   m_isChecked(false),
                                                                                   m_onChange(onChange)
{
    ImGuiStyle &style = ImGui::GetStyle();
    m_size = Style::CalcTextSize(label);
    m_size(0) += Style::GetEM() + 2.0f * style.FramePadding.y;
    m_size(0) += 1.0f * style.ItemInnerSpacing.x;
    m_size(1) += 2.0f * style.FramePadding.y;
}

void Checkbox::render()
{
    if (ImGui::Checkbox(m_label.c_str(), &m_isChecked))
    {
        if (m_onChange)
        {
            m_onChange(m_isChecked);
        }
    }
}

} // namespace orhi::gui