#include <orhi/gui/container.hh>
#include <orhi/gui/widget.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

Container::Container() : m_automaticSizeAdapt(Vector2b::Constant(true))
{
}

Container::Container(const std::vector<std::shared_ptr<Widget>> &widgets) : Container::Container()
{
    this->add(widgets);
}

void Container::add(const std::vector<std::shared_ptr<Widget>> &widgets)
{
    for (const auto &widget : widgets)
    {
        this->add(widget);
    }
}

void Container::add(std::shared_ptr<Widget> widget)
{
    m_widgets.push_back(widget);
    widget->m_insideContainer = true;

    adaptSizeToContent();
}

void Container::adaptSizeToContent()
{
    if (m_automaticSizeAdapt(0))
    {
        adaptWidthToContent();
    }

    if (m_automaticSizeAdapt(1))
    {
        adaptHeightToContent();
    }
}

void Container::adaptHeightToContent()
{
}

void Container::adaptWidthToContent()
{
}

void Container::setSize(Vector2i size)
{
    Widget::setSize(size);
    m_automaticSizeAdapt.setConstant(false);
}

void Container::setSize(int width, int height)
{
    Widget::setSize(width, height);
    m_automaticSizeAdapt.setConstant(false);
}

void Container::setWidth(int width)
{
    Widget::setWidth(width);
    m_automaticSizeAdapt(0) = false;
}

void Container::setHeight(int height) 
{
    Widget::setHeight(height);
    m_automaticSizeAdapt(1) = false;
}

void Container::setRelativeSize(Vector2f size) 
{
    Widget::setRelativeSize(size);
    m_automaticSizeAdapt.setConstant(false);
}

void Container::setRelativeSize(float width, float height) 
{
    Widget::setRelativeSize(width, height);
    m_automaticSizeAdapt.setConstant(false);
}

void Container::setRelativeWidth(float width)
{
    Widget::setRelativeWidth(width);
    m_automaticSizeAdapt(0) = false;
}

void Container::setRelativeHeight(float height) 
{
    Widget::setRelativeHeight(height);
    m_automaticSizeAdapt(1) = false;
}

} // namespace orhi::gui