#include <orhi/gui/button.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

Button::Button(const std::string &label, std::function<void()> onPressed) : m_label(label),
                                                                            m_onPressed(onPressed)
{
    m_size = Style::CalcTextSize(label);
    m_size(0) += 2.0f * ImGui::GetStyle().FramePadding.x;
    m_size(1) += 2.0f * ImGui::GetStyle().FramePadding.y;
}

void Button::render()
{
    if (ImGui::Button((m_label + m_id).c_str()))
    {
        if (m_onPressed)
        {
            m_onPressed();
        }
    }
}

SmallButton::SmallButton(const std::string &label, std::function<void()> onPressed) : m_label(label),
                                                                                      m_onPressed(onPressed)
{
    m_size = Style::CalcTextSize(label);
    m_size(0) += 2.0f * ImGui::GetStyle().FramePadding.x;
}

void SmallButton::render()
{
    if (ImGui::SmallButton((m_label + m_id).c_str()))
    {
        if (m_onPressed)
        {
            m_onPressed();
        }
    }
}

} // namespace orhi::gui
