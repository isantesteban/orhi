#include <orhi/gui/separator.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

void Separator::render()
{
    ImGui::Separator();
}

} // namespace orhi::gui