#include <orhi/gui/style.hh>
#include <orhi/gui/color_picker.hh>
#include <orhi/math/color.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace orhi::gui
{

ColorPicker::ColorPicker(Color *color, const std::string &label, std::function<void(const Color &)> onChange) : m_color(color),
                                                                                                                m_label(label),
                                                                                                                m_onChange(onChange)
{
    m_value = m_color->getRGBA();

    ImGuiStyle &style = ImGui::GetStyle();
    m_size = Style::CalcTextSize(label);
    m_size(0) += 1.0f * style.ItemInnerSpacing.x;
    m_size(1) += 2.0f * style.FramePadding.y;
}

void ColorPicker::render()
{
    bool useFullWidth = m_label.empty() && ImGui::GetCurrentWindow()->DC.ItemWidthStack.empty();

    if (useFullWidth)
    {
        ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
    }

    if (ImGui::ColorEdit3((m_label + m_id).c_str(), m_value.data()))
    {
        if (m_onChange)
        {
            m_color->setRGBA(m_value);
            m_onChange(*m_color);
        }
    }

    if (useFullWidth)
    {
        ImGui::PopItemWidth();
    }
}

} // namespace orhi::gui