#include <orhi/gui/slider.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace orhi::gui
{

Slider::Slider(float *value, Vector2f range, const std::string &label, std::function<void(float)> onChange) : m_value(value),
                                                                                                              m_range(range),
                                                                                                              m_label(label),
                                                                                                              m_onChange(onChange)
{
    ImGuiStyle &style = ImGui::GetStyle();
    m_size = Style::CalcTextSize(label);
    m_size(0) += 1.0f * style.ItemInnerSpacing.x;
    m_size(1) += 2.0f * style.FramePadding.y;
}

void Slider::render()
{
    bool useFullWidth = m_label.empty() && ImGui::GetCurrentWindow()->DC.ItemWidthStack.empty();

    if (useFullWidth)
    {
        ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
    }

    if (ImGui::SliderFloat((m_label + m_id).c_str(), m_value, m_range(0), m_range(1), "%.2f"))
    {
        if (m_onChange)
        {
            m_onChange(*m_value);
        }
    }

    if (useFullWidth)
    {
        ImGui::PopItemWidth();
    }
}

} // namespace orhi::gui