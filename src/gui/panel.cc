#include <orhi/gui/panel.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace orhi::gui
{

Panel::Panel() : Panel::Panel("")
{
}

Panel::Panel(const std::string &title) : m_title(title)
{
    m_flags = ImGuiWindowFlags_NoResize 
            | ImGuiWindowFlags_NoMove 
            | ImGuiWindowFlags_NoCollapse 
            | ImGuiWindowFlags_NoBringToFrontOnFocus;

    if (m_title.empty())
    {
        m_flags |= ImGuiWindowFlags_NoTitleBar;
        m_titleSize.setZero();
    }
    else
    {
        m_titleSize = Style::CalcTextSize(m_title);
        m_titleSize(0) += 2 * ImGui::GetStyle().FramePadding.x;
        m_titleSize(1) += 2 * ImGui::GetStyle().FramePadding.y;
    }

    adaptSizeToContent();

    ImVec4 *colors = ImGui::GetStyle().Colors;
    ImVec4 color = colors[ImGuiCol_WindowBg];
    m_backgroundColor << color.x, color.y, color.z;
}

void Panel::onPreRender()
{
    ImVec2 displaySize = ImGui::GetIO().DisplaySize;
    ImVec2 size = ImVec2((float)m_size(0), (float)m_size(1));
    ImVec2 position = ImVec2((float)m_screenPosition(0), (float)(displaySize.y - m_size(1) - m_screenPosition(1)));
    ImVec4 backgroundColor = ImVec4(m_backgroundColor(0), m_backgroundColor(1), m_backgroundColor(2), 1.0);

    ImGui::SetNextWindowSize(size);
    ImGui::SetNextWindowPos(position);
    ImGui::PushStyleColor(ImGuiCol_WindowBg, backgroundColor);
    ImGui::Begin((m_title + m_id).c_str(), nullptr, m_flags);

    if (!m_enabled)
    {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
    }
}

void Panel::render()
{
    for (auto const &widget : m_widgets)
    {
        widget->onPreRender();
        widget->render();
        widget->onPostRender();
    }
}

void Panel::onPostRender()
{
    if (!m_enabled)
    {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
    }

    ImGui::End();
    ImGui::PopStyleColor();
}

void Panel::adaptWidthToContent()
{
    m_size(0) = Style::GetEM() * 20;
    m_size(0) = std::max(m_size(0), m_titleSize(0));

    for (const auto &widget : m_widgets)
    {
        m_size(0) = std::max(m_size(0), widget->getWidth());
    }

    m_size(0) += 2 * ImGui::GetStyle().WindowPadding.x;
}

void Panel::adaptHeightToContent()
{
    m_size(1) = m_titleSize(1);

    for (const auto &widget : m_widgets)
    {
        m_size(1) += widget->getHeight();
    }

    if (!m_widgets.empty())
    {
        m_size(1) += (m_widgets.size() - 1) * ImGui::GetStyle().ItemSpacing.y;
    }

    m_size(1) += 2 * ImGui::GetStyle().WindowPadding.y;
}

} // namespace orhi::gui