#include <orhi/gui/combo.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace orhi::gui
{

Combo::Combo(const std::vector<std::string> &values,
             const std::string &label,
             std::function<void(std::string)> onChange,
             int currentIndex) : m_values(values),
                                 m_currentIndex(currentIndex),
                                 m_label(label),
                                 m_onChange(onChange)
{

    ImGuiStyle &style = ImGui::GetStyle();
    m_size = Style::CalcTextSize(label);
    m_size(0) += 1.0f * style.ItemInnerSpacing.x;
    m_size(1) += 2.0f * style.FramePadding.y;
}

void Combo::render()
{
    static auto vector_getter = [](void *vec, int idx, const char **out_text) {
        auto &vector = *static_cast<std::vector<std::string> *>(vec);
        if (idx < 0 || idx >= static_cast<int>(vector.size()))
        {
            return false;
        }
        *out_text = vector.at(idx).c_str();
        return true;
    };

    bool useFullWidth = m_label.empty() && ImGui::GetCurrentWindow()->DC.ItemWidthStack.empty();

    if (useFullWidth)
    {
        ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
    }

    if (ImGui::Combo((m_label + m_id).c_str(), &m_currentIndex, vector_getter,
                     static_cast<void *>(&m_values), (int)m_values.size()))
    {
        if (m_onChange)
        {
            m_onChange(m_values[m_currentIndex]);
        }
    }

    if (useFullWidth)
    {
        ImGui::PopItemWidth();
    }
}

} // namespace orhi::gui