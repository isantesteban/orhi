#include <orhi/application/events.hh>
#include <orhi/application/window.hh>

#include <orhi/gui/widget.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace orhi::gui
{

Widget::Widget() : m_anchor(Anchor::TOP_LEFT),
                   m_position(Vector2i::Zero()),
                   m_size(Vector2i::Zero()),
                   m_relativePosition(Vector2f::Zero()),
                   m_relativeSize(Vector2f::Zero()),
                   m_useRelativePosition(Vector2b::Constant(false)),
                   m_useRelativeSize(Vector2b::Constant(false)),
                   m_enabled(true),
                   m_insideContainer(false)
{
    static int widgetCount = 0;
    m_id = "##Widget" + std::to_string(widgetCount++);
}

void Widget::setEnabled(bool enabled)
{
    m_enabled = enabled;
}

void Widget::toggleEnabled()
{
    m_enabled = !m_enabled;
}

bool Widget::isEnabled() const
{
    return m_enabled;
}

Anchor Widget::getAnchor() const
{
    return m_anchor;
}

void Widget::setAnchor(Anchor anchor)
{
    m_anchor = anchor;
}

Vector2i Widget::getPosition() const
{
    return m_position;
}

void Widget::setPosition(Vector2i position)
{
    m_position = position;
    m_useRelativePosition.setConstant(false);
}

void Widget::setPosition(int x, int y)
{
    m_position << x, y;
    m_useRelativePosition.setConstant(false);
}

void Widget::setPositionX(int x)
{
    m_position(0) = x;
    m_useRelativePosition(0) = false;
}

void Widget::setPositionY(int y)
{
    m_position(1) = y;
    m_useRelativePosition(1) = false;
}

Vector2f Widget::getRelativePosition() const
{
    return m_relativePosition;
}

void Widget::setRelativePosition(Vector2f position)
{
    m_relativePosition = position;
    m_useRelativePosition.setConstant(true);
}

void Widget::setRelativePosition(float x, float y)
{
    m_relativePosition << x, y;
    m_useRelativePosition.setConstant(true);
}

void Widget::setRelativePositionX(float x)
{
    m_relativePosition(0) = x;
    m_useRelativePosition(0) = true;
}

void Widget::setRelativePositionY(float y)
{
    m_relativePosition(1) = y;
    m_useRelativePosition(1) = true;
}

Vector2i Widget::getSize() const
{
    return m_size;
}

void Widget::setSize(Vector2i size)
{
    m_size = size;
    m_useRelativeSize.setConstant(false);
}

void Widget::setSize(int width, int height)
{
    m_size << width, height;
    m_useRelativeSize.setConstant(false);
}

int Widget::getWidth() const
{
    return m_size(0);
}

void Widget::setWidth(int width)
{
    m_size(0) = width;
    m_useRelativeSize(0) = false;
}

int Widget::getHeight() const
{
    return m_size(1);
}

void Widget::setHeight(int height)
{
    m_size(1) = height;
    m_useRelativeSize(1) = false;
}

Vector2f Widget::getRelativeSize() const
{
    return m_relativeSize;
}

void Widget::setRelativeSize(Vector2f size)
{
    m_relativeSize = size;
    m_useRelativeSize.setConstant(true);
}

void Widget::setRelativeSize(float width, float height)
{
    m_relativeSize << width, height;
    m_useRelativeSize.setConstant(true);
}

float Widget::getRelativeWidth() const
{
    return m_relativeSize(0);
}

void Widget::setRelativeWidth(float width)
{
    m_relativeSize(0) = width;
    m_useRelativeSize(0) = true;
}

float Widget::getRelativeHeight() const
{
    return m_relativeSize(1);
}

void Widget::setRelativeHeight(float height)
{
    m_relativeSize(1) = height;
    m_useRelativeSize(1) = true;
}

void Widget::onResize(int width, int height)
{
    Vector2i windowSize;
    windowSize << width, height;

    recomputeSize(windowSize);
    recomputePosition(windowSize);
}

void Widget::recomputeSize(Vector2i windowSize)
{
    for (int i = 0; i < m_size.size(); i++)
    {
        if (m_useRelativeSize(i))
        {
            m_size(i) = (int)std::ceil(m_relativeSize(i) * windowSize(i));
        }
    }
}

void Widget::recomputePosition(Vector2i windowSize)
{
    for (int i = 0; i < m_position.size(); i++)
    {
        if (m_useRelativePosition(i))
        {
            m_position(i) = (int)(m_relativePosition(i) * windowSize(i));
        }
    }

    switch (m_anchor)
    {
    case Anchor::TOP_LEFT:
        m_screenPosition(0) = m_position(0);
        m_screenPosition(1) = windowSize(1) - m_size(1) - m_position(1);
        break;
    case Anchor::TOP_CENTER:
        m_screenPosition(0) = (windowSize(0) - m_size(0)) / 2 + m_position(0);
        m_screenPosition(1) = windowSize(1) - m_size(1) - m_position(1);
        break;
    case Anchor::TOP_RIGHT:
        m_screenPosition(1) = windowSize(1) - m_size(1) - m_position(1);
        m_screenPosition(0) = windowSize(0) - m_size(0) - m_position(0);
        break;
    case Anchor::BOTTOM_LEFT:
        m_screenPosition(0) = m_position(0);
        m_screenPosition(1) = m_position(1);
        break;
    case Anchor::BOTTOM_CENTER:
        m_screenPosition(0) = (windowSize(0) - m_size(0)) / 2 + m_position(0);
        m_screenPosition(1) = m_position(1);
        break;
    case Anchor::BOTTOM_RIGHT:
        m_screenPosition(0) = windowSize(0) - m_size(0) - m_position(0);
        m_screenPosition(1) = m_position(1);
        break;
    case Anchor::CENTER_LEFT:
        m_screenPosition(0) = m_position(0);
        m_screenPosition(1) = (windowSize(1) - m_size(1)) / 2 + m_position(1);
        break;
    case Anchor::CENTER_RIGHT:
        m_screenPosition(0) = windowSize(0) - m_size(0) - m_position(0);
        m_screenPosition(1) = (windowSize(1) - m_size(1)) / 2 + m_position(1);
        break;
    case Anchor::CENTER:
        m_screenPosition(0) = (windowSize(0) - m_size(0)) / 2 + m_position(0);
        m_screenPosition(1) = (windowSize(1) - m_size(1)) / 2 + m_position(1);
        break;
    }
}

void Widget::onPreRender()
{
    if (!m_insideContainer)
    {
        int flags = ImGuiWindowFlags_NoResize 
                | ImGuiWindowFlags_NoMove 
                | ImGuiWindowFlags_NoCollapse 
                | ImGuiWindowFlags_NoBringToFrontOnFocus
                | ImGuiWindowFlags_NoBackground
                | ImGuiWindowFlags_NoTitleBar;
                
        ImVec2 displaySize = ImGui::GetIO().DisplaySize;
        ImVec2 size = ImVec2((float)m_size(0), (float)m_size(1));
        ImVec2 position = ImVec2((float)m_screenPosition(0), (float)(displaySize.y - m_size(1) - m_screenPosition(1)));

        ImGui::SetNextWindowSize(size);
        ImGui::SetNextWindowPos(position);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
        ImGui::Begin(m_id.c_str(), nullptr, flags);
    }

    if (!m_enabled)
    {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
    }
}

void Widget::onPostRender()
{
    if (!m_enabled)
    {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
    }

    if (!m_insideContainer)
    {
        ImGui::End();
        ImGui::PopStyleVar();
    }
}

void Widget::bind(std::shared_ptr<Application> application)
{
    // Add resize callback
    auto eventManager = application->getEventManager();
    eventManager->addResizeCallback([this](int width, int height) {
        onResize(width, height);
    });

    // Initialize widget size
    auto window = application->getWindow();
    onResize(window->getWidth(), window->getHeight());
}

Vector3f Widget::getBackgroundColor() const
{
    return m_backgroundColor;
}

void Widget::setBackgroundColor(Vector3f color)
{
    m_backgroundColor = color;
}

} // namespace orhi::gui
