#include <orhi/gui/style.hh>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include "fonts/droid_sans.cpp"

namespace orhi::gui
{

static float g_em = 16.0f;
static float g_scale = 1.0f;

float Style::GetEM()
{
    return g_scale * g_em;
}

Color Style::GetFontColor()
{
    return Color::WHITE;
}

Vector2i Style::CalcTextSize(const std::string &text)
{
    ImFont *font = ImGui::GetDefaultFont();

    Vector2f textSize = Vector2f::Zero();
    float lineWidth = 0.0;
    float lineHeight = font->FontSize;

    for (const char &c : text)
    {
        lineWidth += ((int)c < font->IndexAdvanceX.Size) ? font->IndexAdvanceX[(int)c] : font->FallbackAdvanceX;

        if (c == '\n')
        {
            textSize(0) = std::max(textSize(0), lineWidth);
            textSize(1) += lineHeight;
            lineWidth = 0;
        }
    }

    textSize(0) = std::max(textSize(0), lineWidth);
    textSize(1) += lineHeight;

    return textSize.cast<int>();
}

Style::Style()
{
    m_em = g_em;
    m_scale = g_scale;

    ImGui::StyleColorsClassic();

    apply();
}

void Style::apply()
{
    float em = getEM();

    ImGuiStyle &style = ImGui::GetStyle();

    // Title
    style.WindowTitleAlign = ImVec2(0.5f, 0.5f);

    // Pading and spacing
    style.DisplaySafeAreaPadding = ImVec2(0.0f, 0.0f);
    style.WindowPadding = ImVec2(0.5f * em, 0.5f * em);
    style.FramePadding = ImVec2(0.5f * em, 0.5f * em);
    style.ItemSpacing = ImVec2(0.5f * em, 0.5f * em);
    style.ItemInnerSpacing = ImVec2(0.25f * em, 0.25f * em);
    style.WindowMinSize = ImVec2(em, em);

    // Rounding
    style.WindowRounding = 0.0f;
    style.ChildRounding = 0.0f;
    style.FrameRounding = 3.0f;
    style.ScrollbarRounding = style.FrameRounding;
    style.GrabRounding = style.FrameRounding;

    // Disable borders
    style.WindowBorderSize = 0.0f;
    style.PopupBorderSize = 0.0f;
    style.ChildBorderSize = 0.0f;
    style.FrameBorderSize = 0.0f;

    // Background color
    ImVec4 *colors = ImGui::GetStyle().Colors;
    colors[ImGuiCol_Text] = ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.16, 0.16, 0.16, 1.0f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.16f, 0.16f, 0.16f, 1.0f);
    colors[ImGuiCol_Separator] = ImVec4(0.20, 0.20, 0.20, 1.0f);

    ImGuiIO &io = ImGui::GetIO();
    io.Fonts->Clear();
    io.Fonts->AddFontFromMemoryCompressedTTF(droid_sans_compressed_data, droid_sans_compressed_size, em);
    io.Fonts->Build();
}

float Style::getEM() const
{
    return m_scale * m_em;
}

void Style::setEM(float em)
{
    m_em = em;
    g_em = em;
    apply();
}

float Style::getScale() const
{
    return m_scale;
}

void Style::setScale(float scale)
{
    m_scale = scale;
    g_scale = scale;
    apply();
}

} // namespace orhi::gui