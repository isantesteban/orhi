#include <orhi/gui/viewport.hh>
#include <orhi/graphics/renderer.hh>
#include <orhi/graphics/context.hh>
#include <orhi/scene/cameras/camera.hh>
#include <orhi/scene/cameras/controls/camera_controls.hh>


namespace orhi::gui
{

Viewport::Viewport(std::shared_ptr<Scene> scene, std::shared_ptr<Camera> camera)
{
    m_scene = scene;
    m_camera = camera;
    m_renderer = std::make_shared<graphics::Renderer>();
    m_backgroundColor << 0.7f, 0.7f, 0.7f;
    
    m_relativeSize.setOnes();
    m_useRelativeSize.setConstant(true);
}

void Viewport::render() 
{
    glViewport(m_screenPosition(0), m_screenPosition(1), m_size(0), m_size(1));
    glScissor(m_screenPosition(0), m_screenPosition(1), m_size(0), m_size(1));
    glClearColor(m_backgroundColor(0), m_backgroundColor(1), m_backgroundColor(2), 1.0f);

    m_renderer->render(m_scene, m_camera);
}

void Viewport::onResize(int width, int height)
{
    Widget::onResize(width, height);
    m_camera->setAspectRatio(m_size(0) / (float) m_size(1));
}

void Viewport::bind(std::shared_ptr<Application> application) 
{
    Widget::bind(application);

    auto cameraControls = m_camera->getControls();

    if (cameraControls)
    {
        auto eventManager = application->getEventManager();
        cameraControls->bind(m_camera, eventManager);
    }
}

}