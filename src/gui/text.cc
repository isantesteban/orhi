#include <orhi/gui/text.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

Text::Text(const std::string &text) : Text::Text(text, Style::GetFontColor()) {}

Text::Text(const std::string &text, Color color)
{
    m_text = text;
    m_color = color;
    m_size = Style::CalcTextSize(text);
}

void Text::render()
{
    Vector4f rgba = m_color.getRGBA();
    ImGui::TextColored(ImVec4(rgba(0), rgba(1), rgba(2), rgba(3)), "%s", m_text.c_str());
}

} // namespace orhi::gui