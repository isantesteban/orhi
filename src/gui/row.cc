#include <orhi/gui/row.hh>
#include <orhi/gui/style.hh>
#include <imgui/imgui.h>

namespace orhi::gui
{

Row::Row(){};

Row::Row(const std::vector<std::shared_ptr<Widget>> &widgets)
{
    add(widgets);
}

void Row::render()
{
    float width = (ImGui::GetContentRegionAvailWidth() - m_size(0)) / m_widgets.size();

    ImGui::PushItemWidth(width);
    for (int i = 0; i < m_widgets.size() - 1; i++)
    {
        m_widgets[i]->onPreRender();
        m_widgets[i]->render();
        m_widgets[i]->onPostRender();
        
        ImGui::SameLine();
    }

    m_widgets.back()->render();
    ImGui::PopItemWidth();
}

void Row::adaptWidthToContent()
{
    m_size(0) = 0;

    for (const auto &widget : m_widgets)
    {
        m_size(0) += widget->getWidth();
    }

    if (!m_widgets.empty())
    {
        m_size(0) += (m_widgets.size() - 1) * ImGui::GetStyle().ItemSpacing.x;
    }
}

void Row::adaptHeightToContent()
{
    m_size(1) = 0;

    for (const auto &widget : m_widgets)
    {
        m_size(1) = std::max(m_size(1), widget->getHeight());
    }
}

} // namespace orhi::gui